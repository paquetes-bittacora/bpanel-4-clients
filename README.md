# Clientes Bpanel4

# Instalación por artisan

Simplemente habrá que ejecutar:

```
php artisan bpanel4-clients:install
```

# Instalación manual

Después de instalar el paquete, ejecutar

```
php artisan vendor:publish --provider="Laravel\Fortify\FortifyServiceProvider"
```

Eliminar `app/Actions/Fortify`, porque se usarán las clases que están dentro del
paquete. `app/Providers/FortifyServiceProvider.php` si se mantiene porque está
incluido como provider en el composer.json de Fortify y si no da error 500, pero no se usa.

Es posible que también haya que eliminar `database/migrations/2014_10_12_200000_add_two_factor_columns_to_users_table.php`

# 📅 Eventos

- Al registrarse un cliente, se lanza el evento `\Bittacora\Bpanel4\Clients\Events\UserCreated` (desde `\Bittacora\Bpanel4\Clients\Actions\Fortify\CreateNewUser::create`). Contiene toda la información del formulario de registro.

# 🪝 Hooks

- **registration-after-passwords**: Permite mostrar campos adicionales en el formulario de registro, después de los campos de las contraseñas.

# Tests

Para añadir los tests de este paquete a los tests de Laravel, hay que añadir lo siguiente
a `phpunit.xml` en el bloque de `<testsuites>`

```xml
<!-- Módulo de usuarios de Bpanel4 -->
<testsuite name="Bpanel4UsersUnit">
    <directory suffix="Test.php">./vendor/bittacora/bpanel4-clients/tests/Unit</directory>
</testsuite>
<testsuite name="Bpanel4UsersFeature">
    <directory suffix="Test.php">./vendor/bittacora/bpanel4-clients/tests/Feature</directory>
</testsuite>
```

Después se ejecutarán junto con el resto con el comando

```
php artisan test
```

Para ejecutar solo los tests de este paquete, se puede ejecutar:

```
php artisan test vendor/bittacora/bpanel4-clients/tests/
```

# Notas

Para la validación de DNI uso las reglas de https://github.com/orumad/laravel-spanish-validator/
