<?php

declare(strict_types=1);

return [
    'privacy-policy-page' => '/pagina/politica-de-privacidad',
    'data-protection-view' => 'bpanel4-clients::public.data-protection',
    // Puede ser un 'javascript: ...', un enlace a un encabezado, etc. Dependerá del proyecto y la plantilla.
    'basic-data-protection-link' => '',
    // Id del país seleccionado por defecto en el formulario de registro. Poner a 0 para no seleccionar ninguno
    'default_country_id' => 28,
];