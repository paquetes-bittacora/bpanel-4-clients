<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Database\Factories;

use Bittacora\Bpanel4Users\Database\Factories\UserFactory;
use Bittacora\Bpanel4\Clients\Models\Client;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @method Client create()
 * @method Client createOne()
 * @extends Factory<Client>
 */
final class ClientFactory extends Factory
{
    /**
     * @var class-string<\Bittacora\Bpanel4\Clients\Models\Client>
     */
    protected $model = Client::class;

    /**
     * @return array<string, string>|array<string, \Bittacora\Bpanel4Users\Database\Factories\UserFactory>
     */
    public function definition(): array
    {
        $user = (new UserFactory())->create();
        $user->setActive(1);
        $user->save();
        return [
            'name' => $this->faker->name(),
            'surname' => $this->faker->lastName(),
            'dni' => $this->faker->numberBetween('10000000', '999999999') . $this->faker->randomLetter(),
            'phone' => $this->faker->phoneNumber(),
            'user_id' => $user->id,
        ];
    }
}
