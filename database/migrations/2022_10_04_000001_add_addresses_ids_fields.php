<?php

declare(strict_types=1);

use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    private const TABLE_NAME = 'clients';
    private const SHIPPING_ADDRESS_ID = 'shipping_address_id';
    private const BILLING_ADDRESS_ID = 'billing_address_id';

    public function up(): void
    {
        Schema::table(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->foreignIdFor(ModelAddress::class, self::SHIPPING_ADDRESS_ID)->nullable();
            $table->foreignIdFor(ModelAddress::class, self::BILLING_ADDRESS_ID)->nullable();
        });
    }

    public function down(): void
    {
        Schema::table(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->dropForeign(self::SHIPPING_ADDRESS_ID);
            $table->dropForeign(self::BILLING_ADDRESS_ID);
            $table->dropColumn(self::SHIPPING_ADDRESS_ID);
            $table->dropColumn(self::BILLING_ADDRESS_ID);
        });
    }
};
