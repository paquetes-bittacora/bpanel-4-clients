<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    private const TABLE_NAME = 'wp_client_equivalence';

    public function up(): void
    {
        Schema::create(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->id();
            $table->unsignedBigInteger('bp4_user_id')->nullable();
            $table->unsignedBigInteger('wp_id')->nullable();
        });
    }

    public function down(): void
    {
        Schema::drop(self::TABLE_NAME);
    }
};
