<?php

declare(strict_types=1);

return [
    'client-data-updated' => 'Datos del cliente actualizados',
    'error-updating-client-data' => 'Ocurrió un error al actualizar los datos del cliente',
];
