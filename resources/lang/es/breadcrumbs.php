<?php

declare(strict_types=1);

return [
    'bpanel4-clients' => 'Clientes',
    'index' => 'Listado',
    'edit' => 'Editar',
    'edit-address' => 'Editar dirección',
    'addresses' => 'Direcciones',
];
