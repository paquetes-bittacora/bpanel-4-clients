<?php

declare(strict_types=1);

return [
    'name' => 'Nombre',
    'surname' => 'Apellidos',
    'dni' => 'DNI/CIF/NIE',
    'company-name-label' => 'Empresa (opcional)',
    'email' => 'Email',
    'password' => 'Contraseña',
    'password_confirmation' => 'Confirmar contraseña',
    'log-in' => 'Iniciar sesión',
    'create-account' => 'Crear cuenta',
    'optional' => 'opcional',
    'phone' => 'Teléfono',
    'update-data' => 'Actualizar información',
    'you-registered-as' => 'Usted se ha registrado como',
    'continue' => 'Continuar',
];
