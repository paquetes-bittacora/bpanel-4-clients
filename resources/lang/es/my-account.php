<?php

declare(strict_types=1);

return [
    'data-updated-successfully' => 'Su información se ha actualizado correctamente',
    'error-updating-data' => 'Ocurrió un error al actualizar sus datos',
    'accept-privacy-policy' => 'He leído y entiendo la <a href=":privacy-policy-link" target="_blank">Política de Privacidad</a> y la <a href=":basic-data-protection-link">información básica sobre Protección de Datos</a>',
];
