<?php

declare(strict_types=1);

return [
    'The :attribute must be at least :length characters and contain at least one uppercase character.' =>
        'El :attribute debe tener al menos :length caracteres y contener al menos una letra mayúscula.',
    'The :attribute must be at least :length characters and contain at least one number.' =>
        'El :attribute debe tener al menos :length caracteres y contener al menos un número.',
    'The :attribute must be at least :length characters and contain at least one special character.' =>
        'El :attribute debe tener al menos :length caracteres y contener al menos un símbolo.',
    'The :attribute must be at least :length characters and contain at least one uppercase character and one number.' =>
        'El :attribute debe tener al menos :length caracteres y contener al menos una letra mayúscula y un número.',
    'The :attribute must be at least :length characters and contain at least one uppercase character and one special character.' =>
        'El :attribute debe tener al menos :length caracteres y contener al menos una letra mayúscula y un símbolo.',
    'The :attribute must be at least :length characters and contain at least one uppercase character, one number, and one special character.' =>
        'El :attribute debe tener al menos :length caracteres y contener al menos una letra mayúscula, un número, y un símbolo.',
    'The :attribute must be at least :length characters and contain at least one special character and one number.' =>
        'El :attribute debe tener al menos :length caracteres y contener al menos un símbolo y un número.',
    'The :attribute must be at least :length characters.' => 'El :attribute debe tener al menos :length caracteres.',
];
