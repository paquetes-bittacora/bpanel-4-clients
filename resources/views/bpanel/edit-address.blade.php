@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Edición de dirección: ' . $address->name)
@livewireStyles

@section('content')
    <div class="mb-3 tabs-level tabs-level-1  has-children ">
        <ul class="mt-10 mb-10 nav nav-justified flex-nowrap nav-tabs nav-tabs-simple nav-tabs-faded nav-tabs-detached radius-0 border-1 bgc-grey-l1 brc-grey-l1 pb-0 shadow-sm" role="tablist">
            <li class="nav-item mr-1px">
                <a class="btn bgc-white btn-lighter-grey btn-h-outline-blue btn-a-outline-blue py-2 btn-brc-tp border-none border-t-3 radius-0 letter-spacing active"
                   href="{{ route('bpanel4-clients.edit', ['client' => $address->addressable->id]) }}">
                    <i class="fa fa-pencil text-180 d-block my-1"></i>
                    <span class="text-90">Datos del cliente</span>
                </a>
            </li>
             <li class="nav-item mr-1px">
                <a class="btn bgc-white btn-lighter-grey btn-h-outline-blue btn-a-outline-blue py-2 btn-brc-tp border-none border-t-3 radius-0 letter-spacing active"
                   href="{{ route('bpanel4-clients.addresses', ['client' => $address->addressable]) }}">
                    <i class="fa fa-map-marker text-180 d-block my-1"></i>
                    <span class="text-90">Direcciones</span>
                </a>

            </li>
        </ul>
    </div>
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('user::form.edit') }}</span>
            </h4>
        </div>
        @include('bpanel4-addresses::bpanel.edit-address-form', ['model' => $address])
    </div>
@endsection
@livewireScripts
