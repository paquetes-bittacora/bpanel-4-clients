@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Edición de cliente: ' . $client->getFullName())

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('user::form.edit') }}</span>
            </h4>
        </div>
        @if($errors->any())
            <div class="alert alert-danger m-3">
                {!! implode('', $errors->all('<div>:message</div>')) !!}
            </div>
        @endif

        <form class="mt-lg-3" autocomplete="off" method="post" action="{{route('bpanel4-clients.update', $client)}}">
            @method('PUT')
            @csrf
            @livewire('form::input-text', ['name' => 'name', 'labelText' => __('bpanel4-clients::client.name'), 'required'=>true, 'value' => old('name') ?? $client->name ])
            @livewire('form::input-text', ['name' => 'surname', 'labelText' => __('bpanel4-clients::client.surname'), 'required'=>true, 'value' => old('surname') ?? $client->surname])
            @livewire('form::input-text', ['name' => 'dni', 'labelText' => __('bpanel4-clients::client.dni'), 'required'=>true, 'value' => old('dni') ?? $client->dni])
            @livewire('form::input-text', ['name' => 'company', 'labelText' => __('bpanel4-clients::client.company-name-label'), 'required'=>false, 'value' => old('company') ?? $client->company])
            @livewire('form::input-email', ['name' => 'email', 'labelText' => __('bpanel4-clients::client.email'), 'required'=>true, 'value' => old('email') ?? $client->user->email])
            @livewire('form::input-text', ['name' => 'phone', 'labelText' => __('bpanel4-clients::client.phone'), 'required'=>true, 'value' => old('phone') ?? $client->phone])
            <div class="form-group form-row">
                <div class="col-sm-3 col-form-label text-sm-right"></div>
                <div class="col-sm-7">
                    <strong>Dirección de envío predeterminada</strong>
                </div>
            </div>
            @include('bpanel4-addresses::bpanel.edit-address-fields', ['model' => $client->getShippingAddress()])
            <div class="form-group form-row">
                <div class="col-sm-3 col-form-label text-sm-right"></div>
                <div class="col-sm-7">
                    <hr/>
                </div>
            </div>
            @livewire('form::input-password', ['name' => 'password', 'labelText' => __('bpanel4-clients::client.password'), 'labelWidth' => 3, 'fieldWidth' => 7])
            @livewire('form::input-password', ['name' => 'password_confirmation', 'labelText' => __('bpanel4-clients::client.password_confirmation'), 'labelWidth' => 3, 'fieldWidth' => 7])
            <div class="form-group form-row">
                <div class="col-sm-3 col-form-label text-sm-right"></div>
                <div class="col-sm-7">
                    <em>Deje los campos de contraseña en blanco si no desea cambiar la contraseña del cliente.</em>
                </div>
            </div>

            @livewire('utils::created-updated-info', ['model' => $client])

            @livewire('form::input-checkbox', [
                'name' => 'active',
                'value' => 1,
                'labelText' => 'Activado',
                'bpanelForm' => true,
                'checked' => $client->getUser()->getActive() == 1
            ])
            <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
                @livewire('form::save-button',['theme'=>'update'])
                @livewire('form::save-button',['theme'=>'reset'])
            </div>
            @livewire('form::input-hidden', ['name' => 'client_id', 'value' => $client->id])
        </form>
    </div>

@endsection
