@if(null !== $row->user)
    @livewire('utils::datatable-default', [
        'fieldName' => 'active',
        'model' => $row->user,
        'value' => $row->user->active,
        'size' => 'xxs'
    ], key('active-user-'.$row->id))
@endif