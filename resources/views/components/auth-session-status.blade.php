@props(['status'])

<style>
    .text-green-600 {
        color: rgb(22, 163, 74);
    }

    .text-red-600 {
        color: rgb(220, 38, 38);
    }
</style>

@if ($status)
    <div {{ $attributes->merge(['class' => 'font-medium text-sm text-green-600']) }}>
        {{ $status }}
    </div>
@endif
