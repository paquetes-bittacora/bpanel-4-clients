@extends('bpanel4-public.mail.base-mail')
<?php /**
 * @var \Bittacora\Bpanel4\Clients\Models\Client $client
 */ ?>
@section('preheader')@endsection
@section('content')
    <h2>Su cuenta ha sido activada</h2>
    <p>{{ $client->getName() }}, su cuenta en {{ $shopName }} ha sido activada.</p>
    <p>Ahora podrá iniciar sesión y realizar pedidos.</p>
    <p>Muchas gracias,<br/>{{ $shopName }}</p>
@endsection
