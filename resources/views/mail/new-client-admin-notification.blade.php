@extends('bpanel4-public.mail.base-mail')
<?php /**
 * @var \Bittacora\Bpanel4\Clients\Models\Client $client
 */ ?>
@section('preheader')@endsection
@section('content')
    <h2>Se ha registrado un cliente</h2>
    <p>{{ $client->getFullName() }}, se ha registrado</p>
    <p>Si lo desea, puede ir a su <a href="{{ route('bpanel4-clients.edit', ['client' => $client]) }}">ficha de usuario</a> para revisar sus datos.</p>
@endsection
