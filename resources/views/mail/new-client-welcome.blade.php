@extends('bpanel4-public.mail.base-mail')
<?php /**
 * @var \Bittacora\Bpanel4\Clients\Models\Client $client
 */ ?>
@section('preheader')@endsection
@section('content')
    <h2>Su cuenta en {{ $shopName }} se ha creado</h2>
    <p>{{ $client->getName() }}, gracias por registrarse en {{ $shopName }}.</p>
    <p>Su cuenta ha sido creada y ya puede realizar pedidos en {{ $shopName }} iniciando sesión con el nombre de usuario
       y contraseña que acaba de crear.</p>
    <p>Muchas gracias,<br/>{{ $shopName }}</p>
@endsection
