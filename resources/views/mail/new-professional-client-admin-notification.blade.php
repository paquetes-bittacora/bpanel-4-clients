@extends('bpanel4-public.mail.base-mail')
<?php /**
 * @var \Bittacora\Bpanel4\Clients\Models\Client $client
 */ ?>
@section('preheader')@endsection
@section('content')
    <h2>Se ha registrado un cliente profesional</h2>
    <p>{{ $client->getFullName() }}, se ha registrado como profesional</p>
    <p>Por favor, vaya a su <a href="{{ route('bpanel4-clients.edit', ['client' => $client]) }}">ficha de usuario</a> para activar su cuenta una vez confirme que la información es correcta.</p>
    <p>El cliente recibirá un correo electrónico indicándole que su cuenta ha sido activada.</p>
@endsection
