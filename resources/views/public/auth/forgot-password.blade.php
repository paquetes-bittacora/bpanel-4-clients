@section('meta-title') {{ __('Recuperar contraseña') }} @endsection
@section('meta-description'){{ __('Recuperar contraseña') }}@endsection
@section('meta-keywords'){{ __('recuperar, contraseña') }}@endsection
@extends('bpanel4-public.layouts.regular-page')
@section('content')
    <div class="d-flex justify-content-center align-items-center p-5 col-12 flex-column">
        <x-auth-session-status class="mb-4" :status="session('status')" />
        <form method="POST" action="{{ route('password.email') }}">
            @csrf

            <!-- Email Address -->
            <div class="d-flex flex-column align-items-center">
                <p class="text-center">Introduzca la dirección de email con la que se registró y le enviaremos una nueva contraseña:</p>
                <div class="col-12 d-flex justify-content-center flex-column" style="max-width: 500px;">
                    <div>
                        <strong>
                            <x-input-label for="email" :value="__('Email:')"/>
                        </strong>
                    </div>
                    <x-text-input id="email" class="form-control block mt-1 w-full" type="email" name="email"
                                  :value="old('email')"
                                  required
                                  autofocus/>
                    <x-input-error :messages="$errors->get('email')" class="mt-2"/>
                </div>
            </div>

            <div class="d-flex justify-content-center  mt-4 w-100">
                <x-primary-button class="btn btn-primary recover-password-button">
                    Recuperar contraseña
                </x-primary-button>
            </div>
        </form>
    </div>
@endsection
