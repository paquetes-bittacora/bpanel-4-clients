<?php /** @var \Bittacora\Bpanel4\Clients\Models\Client $client */ ?>
@section('meta-title') {{ __('Iniciar sesión') }} @endsection
@section('meta-description'){{ __('Iniciar sesión') }}@endsection
@section('meta-keywords'){{ __('login,sesion') }}@endsection

@extends('bpanel4-public.layouts.regular-page')
@livewireStyles
@section('content')
    <div class="main-container container bgc-transparent login-form">

        <div class="main-content minh-100 justify-content-center">
            <div class="p-2 p-md-4">
                <div class="row justify-content-center" id="row-1">
                    <div class="bgc-white shadow radius-1 overflow-hidden col-12 col-lg-6 col-xl-5">

                        <div class="row" id="row-2">

                            <div id="id-col-main" class="col-12 py-lg-5 bgc-white px-0">
                                <!-- you can also use these tab links -->

                                <div class="tab-content tab-sliding border-0 p-0" data-swipe="right">
                                    <div class="tab-pane active show mh-100 px-3 px-lg-0 pb-3" id="id-tab-login">
                                        <!-- show this in desktop -->
                                        <!-- show this in mobile device -->
                                        <h2 class="text-center">Iniciar sesión</h2>
                                        <form autocomplete="off" class="form-row mt-4 login-form" method="post"
                                              action="{{route('login')}}">
                                            @csrf
                                            <div class="form-group col-sm-10 offset-sm-1 offset-md-0 col-md-12 row login-email-row">

                                                <div class="@error('email') text-danger brc-red-m2  @else @endif col-12">
                                                    <label for="email" class="w-100">
                                                        <strong>
                                                            <i class="fa fa-user text-grey-m2"></i>
                                                            Email
                                                        </strong>
                                                    </label>
                                                    <input
                                                            type="text"
                                                            class="mt-2 w-100 form-control form-control-lg shadow-none @error('email') is-invalid @endif"
                                                            id="email"
                                                            name="email"
                                                            value="{{ old('email') }}">
                                                </div>
                                                @error('email')
                                                <div class="clearfix"></div>
                                                <div class="invalid-feedback d-block">{{ $message }}</div>
                                                @enderror
                                            </div>

                                            <div class="form-group col-sm-10 offset-sm-1 offset-md-0 col-md-12 mt-4 row login-password-row">
                                                <div class="col-12">
                                                    <label for="email" class="w-100">
                                                        <strong>
                                                            <i class="fa fa-key text-grey-m2 "></i>
                                                            Contraseña
                                                        </strong>
                                                    </label>
                                                    <input type="password"
                                                           class="mt-2 w-100 form-control form-control-lg pr-4 shadow-none @error('password') is-invalid @endif"
                                                           id="password"
                                                           name="password">
                                                </div>
                                                @error('password')
                                                <div class="clearfix"></div>
                                                <div class="invalid-feedback d-block">{{ $message }}</div>
                                                @enderror
                                            </div>

                                            <div class="d-flex flex-column form-group align-items-center col-sm-10 offset-sm-1 col-md-8 offset-md-2">
                                                <label class="d-inline-block mt-3 mb-0 text-dark-l1 text-center w-100">
                                                    <input type="checkbox" class="mr-1" id="remember" name="remember">
                                                    Recuérdame
                                                </label>
                                                <button type="submit"
                                                        class="btn btn-primary btn-block px-4 btn-bold mt-2 mb-4">
                                                    Iniciar sesión
                                                </button>
                                                <div class="">
                                                    <a href="{{ route('password.request') }}">¿Ha olvidado su
                                                                                              contraseña?</a>
                                                </div>
                                                <div class="login-register">
                                                    <a href="{{ route('register') }}">¿No tiene cuenta? <strong>Regístrese
                                                                                                                aquí.</strong></a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div><!-- .tab-content -->
                            </div>


                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div>
            </div>

        </div>
    </div>
@endsection
@livewireScripts
