<?php /** @var \Bittacora\Bpanel4\Clients\Models\Client $client */ ?>

@section('meta-title') {{ __('Crear cuenta') }} @endsection
@section('meta-description'){{ __('Crear cuenta') }}@endsection
@section('meta-keywords'){{ __('crear,cuenta') }}@endsection

@extends('bpanel4-public.layouts.regular-page')
@livewireStyles
@section('content')
    <div class="main-container regular-page-container bgc-transparent">
        <div class="main-content minh-100 justify-content-center">
            <h1 class="mt-4">Cree su cuenta</h1>
            <p>Crear su cuenta en {{ config('app.name') }} es muy sencillo, solo tiene que rellenar el siguiente formulario.</p>
            <form method="POST" action="{{ route('register') }}" class="registration-form">
                @csrf

                <fieldset class="d-flex flex-wrap mt-3 pb-4 w-100">
                    <legend><i class="fa-fw fas fa-user"></i> <strong>Datos personales</strong></legend>
                    <div class="d-flex w-100">
                        <div class="col-md-6 pb-4 px-0 pr-4">
                            @livewire('form::input-text', ['fieldWidth' => 9, 'name' => 'name', 'labelText' =>
                            __('bpanel4-clients::client.name'),
                            'required'=>true])
                        </div>
                        <div class="col-md-6 pb-4 px-0">
                            @livewire('form::input-text', ['fieldWidth' => 9, 'name' => 'surname', 'labelText' =>
                            __('bpanel4-clients::client.surname'), 'required'=>true])
                        </div>
                    </div>
                    <div class="d-flex w-100">
                        <div class="col-md-6 pb-4 px-0 pr-4">
                            @livewire('form::input-text', ['fieldWidth' => 9, 'name' => 'dni', 'labelText' =>
                            __('bpanel4-clients::client.dni'),
                            'required'=>true])
                        </div>
                        <div class="col-md-6 pb-4 px-0">
                            @livewire('form::input-text', ['fieldWidth' => 9, 'name' => 'company', 'labelText' =>
                            __('bpanel4-clients::client.company-name-label'), 'required'=>false])
                        </div>
                    </div>
                    <div class="d-flex w-100">
                        <div class="col-md-6 pb-4 px-0 pr-4">
                            @livewire('form::input-email', ['fieldWidth' => 9, 'name' => 'email', 'labelText' =>
                            __('bpanel4-clients::client.email'), 'required'=>true])
                        </div>
                        <div class="col-md-6 pb-4 px-0">
                            @livewire('form::input-text', ['fieldWidth' => 9, 'name' => 'phone', 'labelText' =>
                            __('bpanel4-clients::client.phone'), 'required'=>true])
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend><i class="fa-fw fas fa-location-circle"></i> <strong>Dirección</strong></legend>
                    <div class="d-flex w-100">
                        <div class="col-md-12 pb-4 px-0">
                            @livewire('form::input-text', ['labelWidth' => 4, 'fieldWidth' => 8 , 'name' =>
                            'address[name]', 'labelText' =>
                            __('bpanel4-addresses::address.name'), 'required'=>true, 'value' => old('address.name'),
                            'placeholder' => __('bpanel4-addresses::address.name-placeholder')])
                        </div>
                    </div>
                    <div class="d-flex w-100">
                        {{-- Nombre de la persona de contacto --}}
                        <div class="col-md-4 pb-4 px-0">
                            @livewire('form::input-text', [
                            'fieldWidth' => 9 ,
                            'idField' => 'person_name',
                            'name' => 'address[person_name]',
                            'labelText' => __('bpanel4-addresses::address.person_name'),
                            'required'=>true,
                            'value' => old('person_name'),
                            'placeholder' => __('bpanel4-addresses::address.person_name_placeholder')
                            ], key('person_name-'. Str::random(10)))
                        </div>
                        {{-- Apellidos de la persona de contacto --}}
                        <div class="col-md-4 pb-4 px-0">
                            @livewire('form::input-text', [
                            'fieldWidth' => 9 ,
                            'idField' => 'person_surname',
                            'name' => 'address[person_surname]',
                            'labelText' => __('bpanel4-addresses::address.person_surname'),
                            'required'=>true,
                            'value' => old('person_surname'),
                            'placeholder' => __('bpanel4-addresses::address.person_surname_placeholder')
                            ], key('person_surname-'. Str::random(10)))
                        </div>
                        {{-- Teléfono de la persona de contacto --}}
                        <div class="col-md-4 pb-4 px-0">
                            @livewire('form::input-text', [
                            'fieldWidth' => 9 ,
                            'idField' => 'person_phone',
                            'name' => 'address[person_phone]',
                            'labelText' => __('bpanel4-addresses::address.person_phone'),
                            'required'=>false,
                            'value' => old('person_phone'),
                            ], key('person_phone-'. Str::random(10)))
                        </div>
                    </div>
                    <div class="d-flex w-100">
                        <div class="col-md-6 pb-4 px-0 d-flex form-group form-row">
                            <div class="col-sm-3 col-form-label text-sm-right ">
                                <label for="id-form-field-1" lass="mb-0  ">
                                    <span class="text-danger">*</span> <strong>País</strong>
                                </label>
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group m-b">
                                    @livewire('country-select', [
                                    'selectedCountry' => old('country', config('bpanel4-clients.default_country_id')),
                                    'required' => true,
                                    ])
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 pb-4 px-0 d-flex form-group form-row">
                            <div class="col-sm-3 col-form-label text-sm-right">
                                <label for="id-form-field-1" class="mb-0  ">
                                    <span class="text-danger">*</span> <strong>Provincia</strong>
                                </label>
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group m-b">
                                    @livewire('state-select', [
                                    'selectedCountry' => old('country',config('bpanel4-clients.default_country_id')),
                                    'selectedState' => old('state', 0),
                                    'required' => true,
                                    ])
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex w-100">
                        <div class="col-md-6 pb-4 px-0 ">
                            @livewire('form::input-text', ['labelWidth' => 3, 'fieldWidth' => 9, 'name' => 'address[location]', 'labelText' =>
                            __('bpanel4-addresses::address.location'), 'required'=>true, 'value' => old('address.location','')])
                        </div>
                        <div class="col-md-6 pb-4 px-0">
                            @livewire('form::input-text', ['labelWidth' => 3, 'fieldWidth' => 9, 'name' => 'address[postal_code]', 'labelText' =>
                            __('bpanel4-addresses::address.postal-code'), 'value' => old('address.postal_code', ''), 'required'=>true])
                        </div>
                    </div>
                    <div class="d-flex w-100">
                        <div class="col-md-12 pb-4 px-0 address-row">
                            @livewire('form::input-text', ['labelWidth' => 12, 'fieldWidth' => 12, 'name' => 'address[address]', 'labelText' =>
                            __('bpanel4-addresses::address.address'), 'required'=>true, 'value' =>
                            old('address.address') ])
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend><i class="fa-fw fas fa-key"></i> <strong>Contraseña</strong></legend>
                    <div class="d-flex w-100">
                        <div class="col-md-6 pb-4 px-0 pr-4">
                            @livewire('form::input-password', ['labelWidth' => 3, 'fieldWidth' => 9, 'name' => 'password', 'labelText' =>
                            __('bpanel4-clients::client.password'), 'required'=>true])
                        </div>
                        <div class="col-md-6 pb-4 px-0">
                            @livewire('form::input-password', ['labelWidth' => 3, 'fieldWidth' => 9, 'name' => 'password_confirmation', 'labelText' =>
                            __('bpanel4-clients::client.password_confirmation'), 'required'=>true])
                        </div>
                    </div>
                </fieldset>

                <div class="registration-after-passwords">
                    {{ Bp4Hook::execute('registration-after-passwords') }}
                </div>

                <div class="registration-legal-checkbox">
                    @livewire('form::input-checkbox', [
                    'name' => 'accept-privacy-policy',
                    'value' => 1,
                    'checked' => false,
                    'labelText' => __('bpanel4-clients::my-account.accept-privacy-policy', [
                    'privacy-policy-link' => config('bpanel4-clients.privacy-policy-page'),
                    'basic-data-protection-link' => config('bpanel4-clients.basic-data-protection-link')
                    ]),
                    'required' => true,
                    'bpanelForm' => true]
                    )
                    @include(config('bpanel4-clients.data-protection-view'))
                </div>

                @if(config('captcha.secret'))
                    <div class="mt-3 d-flex align-items-end flex-column">
                        {!! NoCaptcha::renderJs() !!}
                        {!! NoCaptcha::display() !!}
                        @if ($errors->has('g-recaptcha-response'))
                            <div class="text-danger mt-3">
                                <strong>{{ __('El captcha es obligatorio') }}</strong>
                            </div>
                        @endif
                    </div>
                @endif
                <div class="d-flex justify-content-end mt-4">
                    <button class="btn btn-primary">
                        {{ __('bpanel4-clients::client.create-account'), }}
                    </button>
                </div>
            </form>

        </div>
    </div>
    <script defer>
        // Autocompleto nombre de la dirección con el del cliente por comodidad, pero permito que sean distintos
        const nameField = document.getElementById('name');
        const surnameField = document.getElementById('surname');
        const personNameField = document.getElementById('person_name');
        const personSurameField = document.getElementById('person_surname');

        function bindFields(mainField, secondaryField) {
          mainField.addEventListener('change', function () {
            secondaryField.value = mainField.value;
          });
        }

        bindFields(nameField, personNameField);
        bindFields(surnameField, personSurameField);
    </script>
@endsection
