<?php /** @var \Bittacora\Bpanel4\Clients\Models\Client $client */ ?>


@section('meta-title') {{ __('Recuperar contraseña') }} @endsection
@section('meta-description'){{ __('Recuperar contraseña') }}@endsection
@section('meta-keywords'){{ __('recuperar,contraseña') }}@endsection

@extends('bpanel4-public.layouts.regular-page')
@livewireStyles
@section('content')
    <div class="d-flex justify-content-center align-items-center p-5 col-12 flex-column">
        <form method="POST" action="{{ route('password.update') }}">
            @csrf

            <!-- Password Reset Token -->
            <input type="hidden" name="token" value="{{ $request->route('token') }}">
            <!-- Email Address -->
            <div>
                <div>
                    <strong>
                        <x-input-label for="email" :value="__('Email')"/>
                    </strong>
                </div>
                <x-text-input id="email" class="form-control block mt-1 w-100" type="email" name="email"
                              :value="old('email', $request->email)" required autofocus/>

                <x-input-error :messages="$errors->get('email')" class="mt-2"/>
            </div>

            <!-- Password -->
            <div class="mt-4">
                <div>
                    <strong>
                        <x-input-label for="password" :value="__('Password')"/>
                    </strong>
                </div>
                <x-text-input id="password" class="block mt-1 w-100" type="password" name="password" required/>

                <x-input-error :messages="$errors->get('password')" class="mt-2"/>
            </div>

            <!-- Confirm Password -->
            <div class="mt-4">
                <div>
                    <strong>
                <x-input-label for="password_confirmation" :value="__('Confirm Password')"/>
                    </strong>
                </div>

                <x-text-input id="password_confirmation" class="block mt-1 w-100"
                              type="password"
                              name="password_confirmation" required
                />

                <x-input-error :messages="$errors->get('password_confirmation')" class="mt-2"/>
            </div>

            <div class="d-flex justify-content-center justify-end mt-4">
                <x-primary-button class="btn btn-primary" style="pointer-events: all">
                    {{ __('Reset Password') }}
                </x-primary-button>
            </div>
        </form>
    </div>
@endsection
