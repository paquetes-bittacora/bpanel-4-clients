@extends('bpanel4-public.layouts.regular-page')
@livewireStyles
@section('content')
    <div class="edit-address-form regular-page-container">
        @include('bpanel4-clients::public.my-account.menu')
        <h1>Crear dirección</h1>
        @include('bpanel4-addresses::public.create-address-form', [
            'addressable' => $client,
            'return_route' => $return_route,
        ])
    </div>
@endsection
