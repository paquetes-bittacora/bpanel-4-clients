@extends('bpanel4-public.layouts.regular-page')
@livewireStyles
@section('content')
    <div class="edit-address-form regular-page-container">
        @include('bpanel4-clients::public.my-account.menu')
        <h1>Editar dirección</h1>
        @include('bpanel4-addresses::public.edit-address-form', ['model' => $address])
    </div>
@endsection
