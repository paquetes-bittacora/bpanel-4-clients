<?php /** @var \Bittacora\Bpanel4\Clients\Models\Client $client */ ?>
@extends('bpanel4-public.layouts.regular-page')
@livewireStyles
@section('content')
    <div class="w-100 my-account-details regular-page-container">
        @include('bpanel4-clients::public.my-account.menu')
        <form method="POST" action="{{ route('my-account.update') }}">
            @csrf
            <fieldset class="d-flex flex-wrap mt-3 pb-4">
                <legend><i class="fa-fw fas fa-user"></i> Datos personales</legend>
                <div class="row">
                    <div class="col-md-6 pb-4">
                        @livewire('form::input-text', ['fieldWidth' => 9 , 'name' => 'name', 'labelText' =>
                        __('bpanel4-clients::client.name'),
                        'required'=>true, 'value' => old('name') ?? $client->name ])
                    </div>
                    <div class="col-md-6 pb-4">
                        @livewire('form::input-text', ['fieldWidth' => 9 , 'name' => 'surname', 'labelText' =>
                        __('bpanel4-clients::client.surname'), 'required'=>true, 'value' => old('surname') ??
                        $client->surname])
                    </div>
                    <div class="col-md-6 pb-4">
                        @livewire('form::input-text', ['fieldWidth' => 9 , 'name' => 'dni', 'labelText' =>
                        __('bpanel4-clients::client.dni'),
                        'required'=>true, 'value' => old('dni') ?? $client->dni])
                    </div>
                    <div class="col-md-6 pb-4">
                        @livewire('form::input-text', ['fieldWidth' => 9 , 'name' => 'company', 'labelText' =>
                        __('bpanel4-clients::client.company-name-label'), 'required'=>false, 'value' =>
                        old('company')
                        ??
                        $client->company])
                    </div>
                    <div class="col-md-6 pb-4">
                        @livewire('form::input-email', ['fieldWidth' => 9 , 'name' => 'email', 'labelText' =>
                        __('bpanel4-clients::client.email'),
                        'required'=>true, 'value' => old('email') ?? $client->user->email])
                    </div>
                    <div class="col-md-6 pb-4">
                        @livewire('form::input-text', ['fieldWidth' => 9 , 'name' => 'phone', 'labelText' =>
                        __('bpanel4-clients::client.phone'),
                        'required'=>true, 'value' => old('phone') ?? $client->phone])
                    </div>
                </div>
            </fieldset>
            <fieldset class="d-flex flex-wrap mt-3 pb-4">
                <legend><i class="fa-fw fas fa-location-circle"></i> Dirección</legend>
                <div class="row">
                    @include('bpanel4-addresses::public.edit-address-fields', ['model' => $client->getShippingAddress(), 'prefix' => 'address_'])
                </div>
            </fieldset>
            <fieldset class=" d-flex flex-wrap form-row">
                <legend><i class="fa-fw fas fa-key"></i> Cambiar contraseña</legend>
                <div class="d-flex flex-wrap">
                    <div class="row">
                        <div class="col-12">
                            <i>Rellene estos campos solo si desea cambiar su contraseña.</i>
                        </div>
                        <div class="col-md-6 pb-4">
                            @livewire('form::input-password', ['labelWidth' => 3,'fieldWidth' => 9 , 'name' =>
                            'password',
                            'labelText' =>
                            __('bpanel4-clients::client.password')])
                        </div>
                        <div class="col-md-6 pb-4">
                            @livewire('form::input-password', ['labelWidth' => 3, 'fieldWidth' => 9 , 'name' =>
                            'password_confirmation', 'labelText' =>
                            __('bpanel4-clients::client.password_confirmation')])
                        </div>
                    </div>
                </div>
            </fieldset>
            <div class="d-flex justify-content-end justify-end mt-4  ">
                <button class="btn btn-primary">
                    <i class="fas fa-save"></i> {{ __('bpanel4-clients::client.update-data'), }}
                </button>
            </div>
            @livewire('form::input-hidden', ['name' => 'client_id', 'value' => $client->id])
            @method('PUT')
        </form>
        @if($errors->any())
            No se pudieron actualizar sus datos, por favor, inténtelo de nuevo.
        @endif
    </div>
@endsection
