@extends('bpanel4-public.layouts.regular-page')
@livewireStyles
@section('content')
    <div class="regular-page-container">
        @include('bpanel4-clients::public.my-account.menu')
        <h1>Mis direcciones</h1>
        <div>
            @include('bpanel4-addresses::public.manage-addresses', [
               'model' => $client,
               'editAddressRouteName' => 'my-account.edit-address'
            ])
        </div>
        <div class="add-address-container mt-3 d-flex justify-content-end">
            <a class="btn btn-primary add-address-button"
               href="{{ route('my-account.create-address') }}"
            ><i class="fas fa-plus fa-fw"></i> Añadir una dirección</a>
        </div>
    </div>
@endsection
