<?php /** @var \Bittacora\Bpanel4\HooksComponent\Services\Hooks $hooks */ ?>
@inject("hooks", "Bittacora\Bpanel4\HooksComponent\Services\Hooks")
<div class="my-account-menu">
    <a @if(str_contains(Request::url(), 'direccion')) class="active" @endif href="{{ route('my-account.manage-addresses', ['client' => $client]) }}"><i class="fa-fw far fa-address-book"></i> Direcciones</a>
    <a @if(str_contains(Request::url(), 'pedido')) class="active" @endif href="{{ route('my-account.orders') }}"><i class="fa-fw far fa-shopping-cart"></i> Pedidos</a>
    {!! $hooks->execute('additional-my-account-menu-items') !!}
</div>
