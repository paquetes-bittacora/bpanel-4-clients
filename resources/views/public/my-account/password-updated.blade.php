<?php /** @var \Bittacora\Bpanel4\Clients\Models\Client $client */ ?>
@extends('bpanel4-public.layouts.regular-page')
@livewireStyles
@section('content')
    <div class="w-100 my-account-details regular-page-container text-center">
        <h1>Su contraseña ha sido actualizada</h1>
        <p>¡Gracias por restablecer su contraseña! A partir de ahora, podrá iniciar sesión con la contraseña que acaba de establecer.</p>
        <div class="text-center mt-4">
            <a class="btn btn-primary" href="/">Ir a la portada</a>
        </div>
    </div>
@endsection
