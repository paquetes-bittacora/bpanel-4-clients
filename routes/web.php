<?php

declare(strict_types=1);

use Bittacora\Bpanel4\Clients\Http\Controllers\ClientAdminController;
use Bittacora\Bpanel4\Clients\Http\Controllers\LoginController;
use Bittacora\Bpanel4\Clients\Http\Controllers\MyAccountController;

Route::get('/iniciar-sesion', [LoginController::class, 'index'])->name('client-login')->middleware(['web']);

Route::prefix('mi-cuenta')->name('my-account.')->middleware(['web'])->group(function () {
    Route::get('/', [MyAccountController::class, 'index'])->name('index');
    Route::put('/actualizar', [MyAccountController::class, 'update'])->name('update');
    Route::get('/contrasena-actualizada', [MyAccountController::class, 'passwordUpdated'])->name('password-updated');

    // Direcciones
    Route::get('/{client}/direcciones', [MyAccountController::class, 'manageAddresses'])->name('manage-addresses');
    Route::get('/direcciones/{address}/editar', [MyAccountController::class, 'editAddress'])->name('edit-address');
    Route::get('/direcciones/crear', [MyAccountController::class, 'createAddress'])->name('create-address');
});

Route::prefix('bpanel/clientes')->name('bpanel4-clients.')->middleware(['web', 'auth', 'admin-menu'])
    ->group(function () {
        Route::get('/', [ClientAdminController::class, 'index'])->name('index');
        Route::get('/{client}/editar', [ClientAdminController::class, 'edit'])->name('edit');
        Route::put('/{client}/actualizar', [ClientAdminController::class, 'update'])->name('update');
        Route::get('/{client}/direcciones', [ClientAdminController::class, 'addresses'])->name('addresses');
        Route::get('/direcciones/{address}/editar', [ClientAdminController::class, 'editAddress'])->name('edit-address');
        Route::delete('/{client}/eliminar', [ClientAdminController::class, 'destroy'])->name('destroy');
    });
