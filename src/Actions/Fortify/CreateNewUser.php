<?php

namespace Bittacora\Bpanel4\Clients\Actions\Fortify;

use Bittacora\Bpanel4\Clients\Events\UserCreated;
use Bittacora\Bpanel4Users\Models\User;
use Bittacora\Bpanel4\Clients\UseCases\RegisterNewClient;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Throwable;

final class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    public function __construct(
        private readonly Request $request,
        private readonly RegisterNewClient $registerNewUser
    ) {
    }

    /**
     * Validate and create a newly registered user.
     * @param array<string, string> $input
     * @throws ValidationException
     * @throws Throwable
     */
    public function create(array $input): User
    {
        $user = $this->registerNewUser->execute($input);
        UserCreated::dispatch($input);
        $this->request->session()->flash('success', 'Se ha registrado correctamente, por favor compruebe su
         correo electrónico.');
        return $user;
    }
}
