<?php

namespace Bittacora\Bpanel4\Clients\Actions\Fortify;

use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Validation\Factory;
use Laravel\Fortify\Contracts\ResetsUserPasswords;

final class ResetUserPassword implements ResetsUserPasswords
{
    use PasswordValidationRules;
    /**
     * @var string
     */
    private const PASSWORD = 'password';
    public function __construct(
        private readonly Factory $validationFactory,
        private readonly Hasher $hasher
    ) {
    }

    /**
     * Validate and reset the user's forgotten password.
     *
     * @param  mixed  $user
     */
    public function reset($user, array $input): void
    {
        $this->validationFactory->make($input, [
            self::PASSWORD => $this->passwordRules(),
        ])->validate();

        $user->forceFill([
            self::PASSWORD => $this->hasher->make($input[self::PASSWORD]),
        ])->save();
    }
}
