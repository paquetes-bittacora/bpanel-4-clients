<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Actions\Fortify;

use Bittacora\Bpanel4\Clients\Models\Client;

/**
 * Actualiza los roles de un cliente.
 */
class UpdateClientRoles
{
    public function execute(Client $client, array $input): void
    {
        if ($client->getUser()->hasRole('admin')) {
            $clientRoles[] = 'admin';
        }

        $clientRoles[] = 'any-user';
        $clientRoles[] = 'registered-user';

        $client->getUser()->syncRoles($clientRoles);
    }
}
