<?php

namespace Bittacora\Bpanel4\Clients\Actions\Fortify;

use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Validation\Factory;
use Laravel\Fortify\Contracts\UpdatesUserPasswords;

final class UpdateUserPassword implements UpdatesUserPasswords
{
    use PasswordValidationRules;
    /**
     * @var string
     */
    private const CURRENT_PASSWORD = 'current_password';
    /**
     * @var string
     */
    private const PASSWORD = 'password';
    public function __construct(
        private readonly Factory $validationFactory,
        private readonly Hasher $hasher
    ) {
    }

    /**
     * Validate and update the user's password.
     *
     * @param  mixed  $user
     */
    public function update($user, array $input): void
    {
        $this->validationFactory->make($input, [
            self::CURRENT_PASSWORD => ['required', 'string'],
            self::PASSWORD => $this->passwordRules(),
        ])->after(function ($validator) use ($user, $input): void {
            if (! isset($input[self::CURRENT_PASSWORD]) || ! $this->hasher->check($input[self::CURRENT_PASSWORD], $user->password)) {
                $validator->errors()->add(self::CURRENT_PASSWORD, __('The provided password does not match your current password.'));
            }
        })->validateWithBag('updatePassword');

        $user->forceFill([
            self::PASSWORD => $this->hasher->make($input[self::PASSWORD]),
        ])->save();
    }
}
