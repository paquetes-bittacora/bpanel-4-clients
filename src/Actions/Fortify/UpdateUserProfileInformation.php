<?php

namespace Bittacora\Bpanel4\Clients\Actions\Fortify;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Validation\Factory;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Contracts\UpdatesUserProfileInformation;

final class UpdateUserProfileInformation implements UpdatesUserProfileInformation
{
    /**
     * @var string
     */
    private const NAME = 'name';
    /**
     * @var string
     */
    private const EMAIL = 'email';
    public function __construct(private readonly Factory $validationFactory)
    {
    }
    /**
     * Validate and update the given user's profile information.
     *
     * @param  mixed  $user
     */
    public function update($user, array $input): void
    {
        $this->validationFactory->make($input, [
            self::NAME => ['required', 'string', 'max:255'],

            self::EMAIL => [
                'required',
                'string',
                self::EMAIL,
                'max:255',
                Rule::unique('users')->ignore($user->id),
            ],
        ])->validateWithBag('updateProfileInformation');

        if ($input[self::EMAIL] !== $user->email &&
            $user instanceof MustVerifyEmail) {
            $this->updateVerifiedUser($user, $input);
        } else {
            $user->forceFill([
                self::NAME => $input[self::NAME],
                self::EMAIL => $input[self::EMAIL],
            ])->save();
        }
    }

    /**
     * Update the given verified user's profile information.
     *
     * @param  mixed  $user
     */
    private function updateVerifiedUser($user, array $input): void
    {
        $user->forceFill([
            self::NAME => $input[self::NAME],
            self::EMAIL => $input[self::EMAIL],
            'email_verified_at' => null,
        ])->save();

        $user->sendEmailVerificationNotification();
    }
}
