<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients;

use Bittacora\Bpanel4\Clients\Commands\ConfigurePasswordResetViewCommand;
use Bittacora\Bpanel4\Clients\Commands\PublishConfigCommand;
use Bittacora\Bpanel4\Clients\Commands\PublishViewsCommand;
use Bittacora\Bpanel4\Clients\Contracts\ClientRepository;
use Bittacora\Bpanel4\Clients\Contracts\ClientService;
use Bittacora\Bpanel4\Clients\Actions\Fortify\CreateNewUser;
use Bittacora\Bpanel4\Clients\Actions\Fortify\ResetUserPassword;
use Bittacora\Bpanel4\Clients\Actions\Fortify\UpdateUserPassword;
use Bittacora\Bpanel4\Clients\Actions\Fortify\UpdateUserProfileInformation;
use Bittacora\Bpanel4\Clients\Commands\InstallCommand;
use Bittacora\Bpanel4\Clients\Http\Livewire\ClientsDatatable;
use Bittacora\Bpanel4\Clients\Listeners\PasswordResetListener;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\ServiceProvider;
use Laravel\Fortify\Fortify;
use Livewire\Livewire;

final class Bpanel4ClientsServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->registerCommands();
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views/public/auth', 'auth');
        $this->loadViewsFrom(__DIR__ . '/../resources/views/', 'bpanel4-clients');
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'bpanel4-clients');

        $this->app->bind(ClientService::class, Services\ClientService::class);
        $this->app->bind(ClientRepository::class, Repositories\ClientRepository::class);

        $this->app->booted(function () {
            $this->registerLivewireComponents();
            $this->registerFortifyServices();
            $this->registerFortifyViews();
        });

        $this->publishes([
            __DIR__ . '/../resources/views/components' => resource_path('views/components'),
        ], 'bpanel4-clients-view-components');

        $this->publishes([
            __DIR__.'/../config/bpanel4-clients.php' => config_path('bpanel4-clients.php'),
        ]);
    }

    private function registerFortifyViews(): void
    {
        Fortify::registerView(static fn () => view('auth::register'));
        Fortify::requestPasswordResetLinkView(static fn () => view('auth::forgot-password'));
        Fortify::resetPasswordView(static fn ($request) => view('auth::reset-password', ['request' => $request]));
        Fortify::verifyEmailView(static fn () => view('auth::verify-email'));
    }

    private function registerFortifyServices(): void
    {
        Fortify::createUsersUsing(CreateNewUser::class);
        Fortify::updateUserProfileInformationUsing(UpdateUserProfileInformation::class);
        Fortify::updateUserPasswordsUsing(UpdateUserPassword::class);
        Fortify::resetUserPasswordsUsing(ResetUserPassword::class);

        RateLimiter::for('login', static function (Request $request): Limit {
            $email = (string)$request->email;

            return Limit::perMinute(5)->by($email . $request->ip());
        });

        RateLimiter::for('two-factor', static fn (Request $request) => Limit::perMinute(5)->by($request->session()->get('login.id')));

        \Illuminate\Support\Facades\Event::listen(PasswordReset::class, PasswordResetListener::class);

    }

    private function registerCommands(): void
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                InstallCommand::class,
                PublishViewsCommand::class,
                PublishConfigCommand::class,
                ConfigurePasswordResetViewCommand::class,
            ]);
        }
    }

    private function registerLivewireComponents(): void
    {
        Livewire::component('bpanel4-clients::livewire.clients-table', ClientsDatatable::class);
    }
}
