<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Commands;

use App\Http\Kernel;
use Illuminate\Console\Command;

final class ConfigurePasswordResetViewCommand extends Command
{
    /** @var string */
    protected $signature = 'bpanel4-clients:configure-password-reset-view';

    /** @var string */
    protected $description = '';

    public function handle(): void
    {
        $configFile = base_path('config/fortify.php');
        if (!is_file($configFile)) {
            return;
        }
        $originalContents = file_get_contents($configFile);

        if (str_contains($originalContents, "'password-reset' => 'my-account.password-updated'")) {
            return;
        }

        $newContents = str_replace("'features'", "'redirects' => [
        'password-reset' => 'mi-cuenta/contrasena-actualizada'
    ],
    'features'", $originalContents);
        file_put_contents($configFile, $newContents);
    }
}
