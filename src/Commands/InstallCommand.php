<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Commands;

use Bittacora\AdminMenu\AdminMenu;
use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Bittacora\Bpanel4Users\Models\User;
use Bittacora\Bpanel4\Clients\Models\Client;
use Bittacora\LivewireCountryStateSelector\Models\Country;
use Bittacora\LivewireCountryStateSelector\Models\State;
use Bittacora\Tabs\Tabs;
use Illuminate\Console\Command;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

final class InstallCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'bpanel4-clients:install';

    /**
     * @var string
     */
    protected $description = 'Instala el paquete para gestionar los clientes de Bpanel4.';
    /**
     * @var string[]
     */
    private const PERMISSIONS = ['index', 'create', 'show', 'edit', 'destroy', 'store', 'update', 'addresses'];

    private const REGULAR_USER_PERMISSIONS = [
        'my-account.index',
        'my-account.update',
        'my-account.manage-addresses',
        'my-account.edit-addresses',
        'my-account.edit-address',
    ];

    public function handle(Kernel $artisan, AdminMenu $adminMenu): void
    {
        $this->comment('Instalando el módulo Bpanel4 Clients...');
        $artisan->call('vendor:publish', [
            '--provider' => '"Laravel\Fortify\FortifyServiceProvider"',
        ]);

        $this->createMenuEntries($adminMenu);
        $this->createTabs();

        $this->giveAdminPermissions();
        $this->createUserRoles();
        $this->createClientForAdmin();
        $this->createRegularUsersPermissions();
    }

    private function giveAdminPermissions(): void
    {
        $this->comment('Añadiendo permisos...');
        /** @var Role $adminRole */
        $adminRole = Role::findOrCreate('admin');
        foreach (self::PERMISSIONS as $permission) {
            $permission = Permission::firstOrCreate(['name' => 'bpanel4-clients.'.$permission]);
            $adminRole->givePermissionTo($permission);
        }
    }

    private function createTabs(): void
    {
        Tabs::createItem(
            'bpanel4-clients.edit',
            'bpanel4-clients.edit',
            'bpanel4-clients.edit',
            'Datos del cliente',
            'fa fa-pencil'
        );
        Tabs::createItem(
            'bpanel4-clients.edit',
            'bpanel4-clients.addresses',
            'bpanel4-clients.addresses',
            'Direcciones',
            'fa fa-list-o'
        );
    }


    private function createMenuEntries(AdminMenu $adminMenu): void
    {
        $this->comment('Añadiendo al menú de administración...');

        $adminMenu->createGroup('ecommerce', 'Tienda', 'far fa-shopping-cart');
        $adminMenu->createModule(
            'ecommerce',
            'bpanel4-clients',
            'Gestión de clientes',
            'far fa-users'
        );
        $adminMenu->createAction(
            'bpanel4-clients',
            'Listado',
            'index',
            'fas fa-list'
        );
    }

    private function createUserRoles(): void
    {
        Role::firstOrCreate(['name' => 'any-user']);
        Role::firstOrCreate(['name' => 'registered-user']);
        Role::firstOrCreate(['name' => 'nobody']); // Este rol no debe tenerlo nadie, se usa para hacer que nadie tenga cierto
        // permiso
    }

    private function createClientForAdmin(): void
    {
        DB::transaction(static function (): void {
            $client = new Client();
            $admin = User::whereId(1)->firstOrFail();
            $client->user()->associate($admin);
            $client->name = $admin->getName();
            $client->surname = '';
            $client->dni = '';
            $client->phone = '';
            $client->save();
            $shippingAddress = new ModelAddress();
            $shippingAddress->addressable()->associate($client);
            $shippingAddress->country()->associate(Country::whereName('España')->firstOrFail());
            $shippingAddress->state()->associate(State::whereName('Badajoz')->firstOrFail());
            $shippingAddress->location = '-';
            $shippingAddress->postal_code = '00000';
            $shippingAddress->save();
            $client->getUser()->assignRole('registered-user', 'any-user');
        });
    }

    public function createRegularUsersPermissions(): void
    {
        $this->comment('Añadiendo permisos para los usuarios normales...');
        /** @var Role $role */
        $role = Role::findOrCreate('registered-user');
        foreach (self::REGULAR_USER_PERMISSIONS as $permission) {
            $permission = Permission::firstOrCreate(['name' => $permission]);
            $role->givePermissionTo($permission);
        }
    }
}
