<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Commands;

use Bittacora\AdminMenu\AdminMenu;
use Illuminate\Console\Command;
use Illuminate\Contracts\Console\Kernel;

final class PublishConfigCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'bpanel4-clients:publish-config';

    /**
     * @var string
     */
    protected $description = '';

    public function handle(Kernel $artisan, AdminMenu $adminMenu): void
    {
        $this->call('vendor:publish', ['--provider' => 'Bittacora\Bpanel4\Clients\Bpanel4ClientsServiceProvider']);
    }
}
