<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Commands;

use Bittacora\AdminMenu\AdminMenu;
use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Bittacora\Bpanel4Users\Models\User;
use Bittacora\Bpanel4\Clients\Models\Client;
use Bittacora\LivewireCountryStateSelector\Models\Country;
use Bittacora\LivewireCountryStateSelector\Models\State;
use Bittacora\Tabs\Tabs;
use Illuminate\Console\Command;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

final class PublishViewsCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'bpanel4-clients:publish-views';

    /**
     * @var string
     */
    protected $description = '';

    public function handle(Kernel $artisan, AdminMenu $adminMenu): void
    {
        $artisan->call('vendor:publish --tag=bpanel4-clients-view-components');
    }
}
