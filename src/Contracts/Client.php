<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Contracts;

use Bittacora\Bpanel4\Addresses\Models\ModelAddress;

interface Client
{
    public function getClientId(): int;

    public function getShippingAddress(): ModelAddress;
}
