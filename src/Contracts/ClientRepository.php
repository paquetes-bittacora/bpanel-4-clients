<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Contracts;

interface ClientRepository
{
    public function getById(int $clientId): Client;

    public function getByUserId(int $userId): Client;
}
