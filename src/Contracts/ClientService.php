<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Contracts;

use Bittacora\Bpanel4\Orders\Models\Cart\Cart;

interface ClientService
{
    public function getCurrentClient(): Client;

    public function getClientCart(): Cart;
}
