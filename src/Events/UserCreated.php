<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Evento que se lanza cuando un usuario se registra
 */
final class UserCreated
{
    use Dispatchable, SerializesModels;

    public readonly string $name;
    public readonly string $surname;
    public readonly string $dni;
    public readonly string $company;
    public readonly string $email;
    public readonly string $phone;
    public readonly array $address;
    public readonly int $countryId;
    public readonly int $stateId;
    public readonly string $password;
    public readonly array $others;

    public function __construct(array $data)
    {
        $this->name = $data['name'];
        $this->surname = $data['surname'];
        $this->dni = $data['dni'];
        $this->company = $data['company'] ?? '';
        $this->email = $data['email'];
        $this->phone = $data['phone'];
        $this->address = $data['address'];
        $this->countryId = (int) $data['country'];
        $this->stateId = (int) $data['state'];
        $this->password = $data['password'];

        unset($data['name'], $data['surname'], $data['dni'], $data['company'], $data['email'], $data['phone'],
            $data['address'], $data['country'], $data['state'], $data['password']);

        $this->others = $data;
    }
}