<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Exceptions;

use Exception;

final class UserNotLoggedInException extends Exception
{
    /** @var string */
    protected $message = 'Debe iniciar sesión para realizar esta acción.';
}
