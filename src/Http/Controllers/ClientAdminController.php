<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Bittacora\Bpanel4\Clients\Http\Requests\UpdateClientAdminRequest;
use Bittacora\Bpanel4\Clients\Models\Client;
use Bittacora\Bpanel4\Clients\UseCases\UpdateClient;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Validation\ValidationException;
use Throwable;

final class ClientAdminController extends Controller
{
    public function __construct(
        private readonly Factory $view,
        private readonly Redirector $redirector
    ) {
    }

    public function index(): View
    {
        $this->authorize('bpanel4-clients.index');
        return $this->view->make('bpanel4-clients::bpanel/index');
    }

    public function edit(Client $client): View
    {
        $this->authorize('bpanel4-clients.edit');
        return $this->view->make('bpanel4-clients::bpanel/edit')->with([
            'client' => $client,
        ]);
    }

    /**
     * @throws ValidationException
     */
    public function update(
        UpdateClientAdminRequest $request,
        UpdateClient $updateClient,
        Redirector $redirector
    ): RedirectResponse {
        $this->authorize('bpanel4-clients.update');
        try {
            $updateClient->execute($request->all());
            return $redirector->route('bpanel4-clients.index')
                ->with('alert-success', __('bpanel4-clients::bpanel.client-data-updated'));
        } catch (ValidationException $e) {
            return $redirector->back()->with(
                'alert-danger',
                __('bpanel4-clients::bpanel.error-updating-client-data') . ' ' . $e->getMessage()
            );
        } catch (Throwable $t) {
            return $redirector->back()->with('alert-danger', __('bpanel4-clients::bpanel.error-updating-client-data'));
        }
    }

    public function editAddress(ModelAddress $address): View
    {
        return $this->view->make('bpanel4-clients::bpanel/edit-address')->with([
            'address' => $address,
        ]);
    }

    public function addresses(Client $client): View
    {
        $this->authorize('bpanel4-clients.addresses');

        return $this->view->make('bpanel4-clients::bpanel/addresses')->with([
            'client' => $client,
        ]);
    }

    public function destroy(Client $client): RedirectResponse
    {
        $this->authorize('bpanel4-clients.destroy');

        $client->delete();
        return $this->redirector->back()->with('alert-success', 'Cliente eliminado');
    }
}
