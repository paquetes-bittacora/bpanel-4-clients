<?php

namespace Bittacora\Bpanel4\Clients\Http\Controllers;

use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class LoginController
{
    public function __construct(private readonly Factory $view)
    {
    }

    public function index(): View
    {
        return $this->view->make('bpanel4-clients::public.auth.login');
    }
}
