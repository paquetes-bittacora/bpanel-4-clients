<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Bittacora\Bpanel4\Clients\Exceptions\UserNotLoggedInException;
use Bittacora\Bpanel4\Clients\Http\Requests\UpdateClientRequest;
use Bittacora\Bpanel4\Clients\Models\Client;
use Bittacora\Bpanel4\Clients\Services\ClientService;
use Bittacora\Bpanel4\Clients\UseCases\UpdateClient;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Validation\ValidationException;
use Illuminate\View\Factory;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

final class MyAccountController extends Controller
{
    public function __construct(
        private readonly Factory $viewFactory,
        private readonly ClientService $clientService
    ) {
    }

    /**
     * @throws UserNotLoggedInException
     */
    public function index(): View
    {
        $this->authorize('my-account.index');
        return $this->viewFactory->make('bpanel4-clients::public/my-account.index', [
            'client' => $this->clientService->getCurrentClient(),
        ]);
    }

    /**
     * @throws ValidationException
     */
    public function update(
        UpdateClientRequest $request,
        UpdateClient $updateClient,
        Redirector $redirector
    ): RedirectResponse {
        $this->authorize('my-account.update');
        try {
            $requestData = $request->all();
            $requestData['active'] = 1;
            $updateClient->execute($requestData);
            return $redirector->route('my-account.index')
                ->with('success', __('bpanel4-clients::my-account.data-updated-successfully'));
        } catch (ValidationException $e) {
            throw $e;
        } catch (Throwable) {
            return $redirector->back()->with('error', __('bpanel4-clients::my-account.error-updating-data'));
        }
    }

    public function manageAddresses(Client $client): View
    {
        $this->checkUser($client);

        $this->authorize('my-account.manage-addresses');

        return $this->viewFactory->make('bpanel4-clients::public/my-account.manage-addresses')->with([
            'client' => $client,
        ]);
    }

    public function editAddress(ModelAddress $address, ClientService $clientService): View
    {
        $this->authorize('my-account.edit-addresses');
        $this->checkUser($address->addressable()->firstOrFail());

        return $this->viewFactory->make('bpanel4-clients::public/my-account.edit-address')->with([
            'address' => $address,
            'client'=> $clientService->getCurrentClient(),
        ]);
    }

    public function createAddress(ClientService $clientService): View
    {
        $client = $clientService->getCurrentClient();
        return $this->viewFactory->make('bpanel4-clients::public/my-account.create-address')->with([
            'client'=> $client,
            'return_route' => route('my-account.manage-addresses', ['client' => $client]),
        ]);
    }

    public function passwordUpdated(): View
    {
        return $this->viewFactory->make('bpanel4-clients::public/my-account.password-updated');
    }

    private function checkUser(Client $client): void
    {
        if ($client->getClientId() !== $this->clientService->getCurrentClient()->getClientId()) {
            throw new HttpException(403);
        }
    }
}
