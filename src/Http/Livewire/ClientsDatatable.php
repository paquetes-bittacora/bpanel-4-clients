<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Http\Livewire;

use Bittacora\Bpanel4\Clients\Models\Client;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

final class ClientsDatatable extends DataTableComponent
{
    /**
     * @return array<Column>
     */
    public function columns(): array
    {
        return [
            Column::make('Nombre', 'name')->searchable()->sortable(),
            Column::make('Apellidos', 'surname')->searchable()->sortable(),
            Column::make('Correo electrónico', 'user.email')->searchable()->sortable(),
            Column::make('Empresa', 'company')->searchable()->sortable(),
            Column::make('NIF/CIF', 'dni')->searchable()->sortable(),
            Column::make('País', 'shippingAddress.country.name')->searchable()->sortable(),
            Column::make('Provincia', 'shippingAddress.state.name')->searchable()->sortable(),
            Column::make('Ciudad', 'shippingAddress.location')->searchable()->sortable(),
            Column::make('Fecha de alta', 'created_at')->sortable(),
            Column::make('Activo', 'user.active')->view('bpanel4-clients::bpanel.livewire.datatable-columns.active'),
            Column::make('Acciones', 'id')->view('bpanel4-clients::bpanel.livewire.datatable-columns.actions'),
        ];
    }

    /**
     * @return Builder<Client>
     */
    public function query(): Builder
    {
        return Client::query()
            ->select('user_id')
            ->with('user')
            ->whereNot('is_guest', '=', 1)
            ->orderBy('created_at', 'DESC')
            ->when($this->getAppliedFilterWithValue('search'), fn($query, $term) => $query
                ->where('name', 'like', '%' . $term . '%')
                ->orWhere('surname', 'like', '%' . $term . '%')
                ->orWhere('company', 'like', '%' . $term . '%')
                ->orWhere('dni', 'like', '%' . $term . '%')
                ->orWhereRelation('user', 'email', 'like', '%' . $term . '%')
                ->orWhereRelation('mainAddress', 'location', 'like', '%' . $term . '%'));
    }

    public function rowView(): string
    {
        return 'bpanel4-clients::bpanel.livewire.client-datatable';
    }

    /**
     * @return string[]
     */
    public function bulkActions(): array
    {
        return [
            'bulkDelete' => 'Eliminar',
        ];
    }

    public function bulkDelete(): void
    {
        foreach ($this->getSelected() as $clientId) {
            Client::destroy((int) $clientId);
        }
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    public function builder(): Builder
    {
        return $this->query();
    }
}
