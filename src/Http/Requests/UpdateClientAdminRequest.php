<?php

namespace Bittacora\Bpanel4\Clients\Http\Requests;

use Bittacora\Bpanel4\Clients\Exceptions\UserNotLoggedInException;
use Bittacora\Bpanel4\Clients\Validation\ClientValidator;
use Illuminate\Auth\AuthManager;
use Illuminate\Foundation\Http\FormRequest;

final class UpdateClientAdminRequest extends FormRequest
{
    public function __construct(private readonly AuthManager $auth)
    {
        parent::__construct();
    }

    /**
     * Solo autorizo la petición si el usuario logueado se corresponde con el cliente que se intenta editar.
     *
     * @throws UserNotLoggedInException
     */
    public function authorize(): bool
    {
        return $this->auth->user()->hasPermissionTo('bpanel4-clients.update');
    }

    /**
     * @return array<string, mixed>
     */
    public function rules(ClientValidator $clientValidation): array
    {
        return $clientValidation->getClientUpdateAdminValidationFields($this->request->getInt('client_id'));
    }
}
