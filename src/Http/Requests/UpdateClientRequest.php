<?php

namespace Bittacora\Bpanel4\Clients\Http\Requests;

use Bittacora\Bpanel4\Clients\Exceptions\UserNotLoggedInException;
use Bittacora\Bpanel4\Clients\Services\ClientService;
use Bittacora\Bpanel4\Clients\Validation\ClientValidator;
use Illuminate\Foundation\Http\FormRequest;

final class UpdateClientRequest extends FormRequest
{
    /**
     * Solo autorizo la petición si el usuario logueado se corresponde con el cliente que se intenta editar.
     *
     * @throws UserNotLoggedInException
     */
    public function authorize(ClientService $clientService): bool
    {
        return $clientService->getCurrentClient()->id === $this->request->getInt('client_id');
    }

    /**
     * @return array<string, mixed>
     */
    public function rules(ClientValidator $clientValidation): array
    {
        return $clientValidation->getClientUpdateValidationFields($this->request->getInt('client_id'));
    }
}
