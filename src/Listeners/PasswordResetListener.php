<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Listeners;

use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

final class PasswordResetListener
{
    public function handle(PasswordReset $event): RedirectResponse
    {
        Auth::login($event->user);

        return redirect()->to('/')->with('alert-success', 'Contraseña reestablecida');
    }
}