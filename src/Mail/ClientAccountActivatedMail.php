<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Mail;

use Bittacora\Bpanel4\Clients\Models\Client;
use Illuminate\Mail\Mailable;

final class ClientAccountActivatedMail extends Mailable
{
    public function __construct(private readonly Client $client)
    {
    }

    public function build(): ClientAccountActivatedMail
    {
        return $this->subject('Su cuenta en ' . config('app.name') . ' ha sido activada')
            ->view('bpanel4-clients::mail.account-activated', [
                'client' => $this->client,
                'shopName' => config('app.name'),
            ]);
    }
}
