<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Mail;

use Bittacora\Bpanel4\Clients\Models\Client;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Mail\Mailable;

final class NewClientRegistrationMail extends Mailable
{
    public function __construct(
        private readonly Client $client,
        private readonly Repository $config
    ) {
    }

    public function build(): NewClientRegistrationMail
    {
        return $this->subject('Su cuenta en ' . $this->config->get('app.name'))
            ->view('bpanel4-clients::mail.new-client-welcome', [
                'client' => $this->client,
                'shopName' => $this->config->get('app.name'),
            ]);
    }
}
