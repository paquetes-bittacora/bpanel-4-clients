<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Mail;

use Bittacora\Bpanel4\Clients\Models\Client;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Mail\Mailable;

final class NewProfessionalClientRegistrationAdminMail extends Mailable
{
    public function __construct(
        private readonly Client $client,
        private readonly Repository $config
    ) {
    }

    public function build(): NewProfessionalClientRegistrationAdminMail
    {
        return $this->subject('Se ha registrado un cliente profesional')
            ->view('bpanel4-clients::mail.new-professional-client-admin-notification', [
                'client' => $this->client,
                'shopName' => $this->config->get('app.name'),
            ]);
    }
}
