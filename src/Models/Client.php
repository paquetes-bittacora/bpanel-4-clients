<?php

namespace Bittacora\Bpanel4\Clients\Models;

use Bittacora\Bpanel4\Orders\Contracts\CartableClient;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Orders\Traits\HasCartTrait;
use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Bittacora\Bpanel4\Addresses\Traits\ModelWithAddressesTrait;
use Bittacora\Bpanel4Users\Models\User;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * Bittacora\Bpanel4\Clients\Models\Client
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $surname
 * @property string $dni
 * @property string|null $company
 * @property string $phone
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int $shipping_address_id
 * @property ?int $billing_address_id
 * @property-read User $user
 * @method static Builder|Client newModelQuery()
 * @method static Builder|Client newQuery()
 * @method static Builder|Client query()
 * @method static Builder|Client whereCompany($value)
 * @method static Builder|Client whereCreatedAt($value)
 * @method static Builder|Client whereDni($value)
 * @method static Builder|Client whereId($value)
 * @method static Builder|Client whereName($value)
 * @method static Builder|Client wherePhone($value)
 * @method static Builder|Client whereSurname($value)
 * @method static Builder|Client whereUpdatedAt($value)
 * @method static Builder|Client whereUserId($value)
 * @property Carbon|null $deleted_at
 * @property-read Collection|ModelAddress[] $addresses
 * @property-read int|null $addresses_count
 * @method static \Illuminate\Database\Query\Builder|Client onlyTrashed()
 * @method static Builder|Client whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Client withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Client withoutTrashed()
 * @mixin Eloquent
 * @property-read Cart|null $cart
 */
final class Client extends Model implements CartableClient, \Bittacora\Bpanel4\Clients\Contracts\Client
{
    use HasFactory;
    use SoftDeletes;
    use ModelWithAddressesTrait;
    use HasCartTrait;

    /**
     * @var string[]
     */
    protected $fillable = [
        'user_id',
        'name',
        'surname',
        'dni',
        'company',
        'phone',
    ];

    /**
     * @return BelongsTo<User, Client>
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function getUser(): User
    {
        /** @var User $user */
        $user = $this->user()->firstOrFail();

        return $user;
    }

    public function getFullName(): string
    {
        return $this->name . ' ' . $this->surname;
    }

    public function getClientId(): int
    {
        return $this->id;
    }

    public function getClient(): Model&CartableClient
    {
        return $this;
    }

    public function setShippingAddress(ModelAddress $modelAddress): void
    {
        $this->shippingAddress()->associate($modelAddress)->save();
    }

    public function getShippingAddress(): ModelAddress
    {
        /** @var ?ModelAddress $shippingAddress */
        $shippingAddress = $this->shippingAddress()->first();

        return $shippingAddress ?? $this->addresses()->get()->firstOrFail();
    }

    public function setBillingAddress(ModelAddress $modelAddress): void
    {
        $this->billingAddress()->associate($modelAddress)->save();
    }

    public function getBillingAddress(): ModelAddress
    {
        /** @var ?ModelAddress $billingAddress */
        $billingAddress = $this->billingAddress()->first();
        return $billingAddress ?? $this->getShippingAddress();
    }

    /**
     * @return BelongsTo<ModelAddress, Client>
     */
    public function shippingAddress(): BelongsTo
    {
        return $this->belongsTo(ModelAddress::class);
    }

    /**
     * @return BelongsTo<ModelAddress, Client>
     */
    public function billingAddress(): BelongsTo
    {
        return $this->belongsTo(ModelAddress::class);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLastName(): string
    {
        return $this->surname;
    }

    public function getNif(): string
    {
        return $this->dni;
    }

    public function getCompany(): string
    {
        return $this->company ?? '';
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getEmail(): string
    {
        return $this->getUser()->getEmail();
    }

    public function delete(): void
    {
        // Para evitar que no se puedan crear o editar clientes con estos mismos datos
        $this->dni .= '_DELETED';
        $this->save();
        $user = $this->getUser();
        parent::delete();
        $user->name .= '_DELETED';
        $user->email .= '_DELETED';
        $user->save();
    }
}
