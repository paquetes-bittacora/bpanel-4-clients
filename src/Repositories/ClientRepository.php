<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Repositories;

use Bittacora\Bpanel4\Clients\Models\Client;

final class ClientRepository implements \Bittacora\Bpanel4\Clients\Contracts\ClientRepository
{
    public function getById(int $clientId): Client
    {
        return Client::findOrFail($clientId);
    }

    public function getByUserId(int $userId): Client
    {
        return Client::where('user_id', $userId)->firstOrFail();
    }
}
