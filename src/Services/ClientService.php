<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Services;

use Bittacora\Bpanel4\Clients\Exceptions\UserNotLoggedInException;
use Bittacora\Bpanel4\Clients\UseCases\RegisterNewClient;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Orders\Services\ClientCartService;
use Bittacora\Bpanel4Users\Models\User;
use Bittacora\Bpanel4\Clients\Models\Client;
use Bittacora\Bpanel4\Clients\Repositories\ClientRepository;
use Illuminate\Auth\AuthManager;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Servicio para funciones relacionadas con el cliente.
 */
class ClientService implements \Bittacora\Bpanel4\Clients\Contracts\ClientService
{
    public function __construct(
        private readonly AuthManager $auth,
        private readonly ClientRepository $clientRepository,
        private readonly ClientCartService $clientCartService,
        private readonly RegisterNewClient $registerNewClient,
    ) {
    }

    /**
     * Obtiene el cliente correspondiente al usuario conectado actualmente.
     * @throws UserNotLoggedInException
     */
    public function getCurrentClient(): Client
    {
        $currentUser = $this->auth->user();

        if (!is_a($currentUser, User::class)) {
            throw new UserNotLoggedInException();
        }

        return $this->clientRepository->getByUserId($currentUser->getId());
    }

    /**
     * @throws UserNotLoggedInException
     */
    public function getClientCart(): Cart
    {
        try {
            return $this->clientCartService->get($this->getCurrentClient());
        } catch (UserNotLoggedInException) {
            return $this->clientCartService->getAnonymousCart();
        } catch (ModelNotFoundException) {
            $user = $this->auth->user();
            $this->registerNewClient->execute([
                'name' => $user->name,
                'email' => $user->email,
                'surname' => '-',
                'dni' => '-',
                'company' => '-',
                'phone' => '-',
                'country' => 28,
                'state' => 649,
                'address' => [
                    'name' => 'Casa',
                    'location' => 'Badajoz',
                    'address' => '-',
                    'postal_code' => '00000',
                ]
            ], $user);
            return $this->getClientCart();
        }
    }

    public function getClientTypeName(): string
    {
        return 'any-user';
    }
}
