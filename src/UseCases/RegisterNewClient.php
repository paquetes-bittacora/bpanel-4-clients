<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\UseCases;

use Bittacora\Bpanel4\Addresses\UseCases\CreateAddress;
use Bittacora\Bpanel4Users\Models\User;
use Bittacora\Bpanel4\Clients\Actions\Fortify\UpdateClientRoles;
use Bittacora\Bpanel4\Clients\Mail\NewClientRegistrationMail;
use Bittacora\Bpanel4\Clients\Mail\NewClientRegistrationAdminMail;
use Bittacora\Bpanel4\Clients\Models\Client;
use Bittacora\Bpanel4\Clients\Validation\ClientValidator;
use Exception;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Database\Connection;
use Illuminate\Validation\ValidationException;
use Throwable;

/**
 * Registra un nuevo usuario.
 */
final class RegisterNewClient
{
    public function __construct(
        private readonly Connection $db,
        private readonly ClientValidator $clientValidation,
        private readonly UpdateClientRoles $updateClientRoles,
        private readonly CreateAddress $createAddress,
        private readonly Mailer $mailer,
        private readonly Repository $config,
    ) {
    }

    /**
     * @param array<string, string> $input
     * @throws Throwable
     * @throws ValidationException
     */
    public function execute(array $input, ?User $user = null): User
    {
        $this->db->beginTransaction();

        try {
            $this->clientValidation->validateRegistrationData($input, $user);
            $user = $this->createUser($input, $user);
            $this->db->commit();
            $client = Client::whereUserId($user->getId())->firstOrFail();
            $this->sendClientEmail($client);
            $this->sendAdminEmail($client);
            return $user;
        } catch (Exception $exception) {
            report($exception);
            $this->db->rollBack();
            throw $exception;
        }
    }

    /**
     * @param array<string, string> $input
     */
    private function createUser(array $input, ?User $user = null): User
    {
        $user = $user ?? User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => $input['password'],
        ]);
        $user->setActive(1);
        $client = $this->createClient($user, $input);
        $address = $this->createAddress->handle($input, Client::class, $client->getClientId());
        $client->shippingAddress()->associate($address)->save();
        $client->billingAddress()->associate($address)->save();
        $this->updateClientRoles->execute($client, $input);
        return $user;
    }

    /**
     * @param array<string, string> $input
     */
    private function createClient(User $user, array $input): Client
    {
        return Client::create([
            'user_id' => $user->getId(),
            'name' => $input['name'],
            'surname' => $input['surname'],
            'dni' => $input['dni'],
            'company' => $input['company'],
            'phone' => $input['phone'],
        ]);
    }

    private function sendClientEmail(Client $client): void
    {
        $mailable = new NewClientRegistrationMail($client, $this->config);
        $this->mailer->to($client->getEmail())->send($mailable);
    }

    private function sendAdminEmail(Client $client): void
    {
        $mailable = new NewClientRegistrationAdminMail($client, $this->config);
        $this->mailer->to($this->config->get('bpanel4.admin_email'))->send($mailable);
    }
}
