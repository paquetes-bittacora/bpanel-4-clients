<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\UseCases;

use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Clients\Actions\Fortify\UpdateClientRoles;
use Bittacora\Bpanel4\Clients\Mail\ClientAccountActivatedMail;
use Bittacora\Bpanel4\Clients\Models\Client;
use Bittacora\Bpanel4\Clients\Validation\ClientValidator;
use Exception;
use Illuminate\Database\Connection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;
use Throwable;

/**
 * Actualiza los datos del cliente.
 */
final class UpdateClient
{
    public function __construct(
        private readonly Connection $db,
        private readonly ClientValidator $clientValidation,
        private readonly UpdateClientRoles $updateClientRoles,
    ) {
    }

    /**
     * @param array<string, string> $input
     * @throws Throwable
     * @throws ValidationException
     */
    public function execute(array $input): void
    {
        $this->db->beginTransaction();

        try {
            $this->clientValidation->validateUpdateData($input);
            $this->updateUser($input);
            $this->db->commit();
        } catch (Exception $exception) {
            $this->db->rollBack();
            throw $exception;
        }
    }

    /**
     * @param array<string, string> $data
     */
    public function updateUser(array $data): void
    {
        /** @var Client $client */
        $client = Client::findOrFail((int)$data['client_id']);
        $user = $client->getUser();
        $clientWasActive = (bool) $user->getActive();

        $client->name = $data['name'];
        $client->surname = $data['surname'];
        $client->dni = $data['dni'];
        $client->company = $data['company'];
        $client->phone = $data['phone'];

        $user->setEmail($data['email']);
        $user->setName($data['name']);
        $user->setActive((int) ($data['active'] ?? 0));

        if (isset($data['password']) && '' !== $data['password']) {
            $user->setPassword(Hash::make($data['password']));
        }

        $address = $client->getShippingAddress();
        $address->name = $data['address']['name'];
        $address->country_id = $data['country'];
        $address->state_id = $data['state'];
        $address->location = $data['address']['location'];
        $address->postal_code = $data['address']['postal_code'];
        $address->address = $data['address']['address'];

        $address->save();
        $user->save();

        $this->sendClientAccountActivatedMail($client, $clientWasActive);

        $client->save();
    }

    public function sendClientAccountActivatedMail(Client $client, bool $clientWasActive): void
    {
        if (false === $clientWasActive && 1 === $client->getUser()->getActive()) {
            $mailable = new ClientAccountActivatedMail($client);
            Mail::to($client->getEmail())->send($mailable);
        }
    }

}
