<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Validation;

use Bittacora\Bpanel4Users\Models\User;
use Bittacora\Bpanel4\Clients\Actions\Fortify\PasswordValidationRules;
use Bittacora\Bpanel4\Clients\Models\Client;
use Bittacora\Bpanel4\Clients\Repositories\ClientRepository;
use Illuminate\Validation\Factory;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Laravel\Fortify\Rules\Password;

final class ClientValidator
{
    use PasswordValidationRules;

    public function __construct(
        private readonly Factory $validatorFactory,
        private readonly ClientRepository $clientRepository
    ) {
    }

    /**
     * @param array<string, string> $input
     * @throws ValidationException
     */
    public function validateRegistrationData(array $input, $user = null): void
    {
        $this->validatorFactory->make($input, $this->getRegistrationValidationFields($user))->validate();
    }

    /**
     * @return array<string, array<mixed>>
     */
    private function getRegistrationValidationFields($user = null): array
    {
        $rules = [
            // Usuario
            'name' => ['required', 'string', 'max:255'],
            // Cliente
            'surname' => ['required', 'string', 'max:255'],
            'company' => ['string', 'max:255', 'nullable'],
            'phone' => ['required', 'string', 'max:20'],
            // Dirección
            'address.name' => ['required', 'string', 'max:255'],
            'address.person_name' => ['required', 'string', 'max:255'],
            'address.person_surname' => ['required', 'string', 'max:255'],
            'address.person_phone' => ['nullable', 'string', 'max:255'],
            'country' => ['required'],
            'state' => ['required'],
            'address.location' => ['required', 'string', 'max:255'],
            'address.address' => ['required', 'string', 'max:255'],
            'address.postal_code' => ['required', 'string', 'min:0'],
        ];

        if (config('captcha.secret')) {
            $rules['g-recaptcha-response'] = ['required', 'captcha'];
        }

        if (null === $user) {
            $rules['email'] = ['required', 'string', 'email', 'max:255', Rule::unique(User::class)];
            $rules['password'] = $this->passwordRules();
            $rules['dni'] = ['required', resolve(NifValidator::class), Rule::unique(Client::class)];
        }

        return $rules;
    }

    /**
     * @param array<string, string> $input
     * @throws ValidationException
     */
    public function validateUpdateData(array $input): void
    {
        $this->validatorFactory->make($input, $this->getClientUpdateValidationFields((int)$input['client_id']))
            ->validate();
    }

    /**
     * @param int $clientId Necesario para ignorar el email del usuario en la regla 'unique' de los campos email y DNI.
     * @return array<string, mixed>
     */
    public function getClientUpdateValidationFields(int $clientId): array
    {
        $client = $this->clientRepository->getById($clientId);

        return [
            // Usuario
            'name' => ['required', 'string', 'max:255'],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique(User::class)->ignore($client->user->getId()),
            ],
            'password' => ['nullable', 'string', new Password(), 'confirmed'],
            // Cliente
            'client_id' => ['required', 'numeric'],
            'surname' => ['required', 'string', 'max:255'],
            'dni' => ['required', 'spanish_tax_number', Rule::unique(Client::class, 'dni')->ignore($client->id)],
            'company' => ['string', 'max:255', 'nullable'],
            'phone' => ['required', 'string', 'max:20'],
            // Dirección
            'address.name' => ['required', 'string', 'max:255'],
            'country' => ['required'],
            'state' => ['required'],
            'address.location' => ['required', 'string', 'max:255'],
            'address.address' => ['required', 'string', 'max:255'],
            'address.postal_code' => ['required', 'numeric', 'max:999999', 'min:0'],
        ];
    }

    /**
     * @return array<string, mixed>
     */
    public function getClientUpdateAdminValidationFields(int $clientId): array
    {
        $fields = $this->getClientUpdateValidationFields($clientId);
        $fields['client_id'] = ['required', 'numeric'];
        $fields['active'] = ['numeric', 'nullable'];
        return $fields;
    }
}
