<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Validation;

use Bittacora\LivewireCountryStateSelector\Models\Country;
use Illuminate\Contracts\Validation\DataAwareRule;
use Illuminate\Contracts\Validation\InvokableRule;
use Orumad\SpanishValidator\Validator;

final class NifValidator implements InvokableRule, DataAwareRule
{
    /** @var array<string>  */
    private array $data;

    public function __construct(
        private readonly Validator $nifValidator
    ) {
    }

    public function __invoke($attribute, $value, $fail): void
    {
        if ($this->countryIsNotSpain()) {
            return;
        }

        if ($this->dniIsNotValid($value)) {
            $fail('El campo DNI debe ser un NIF/CIF/NIE válido (valor recibido: ' . $value . ')');
        }
    }

    /**
     * @param $data array<string>
     */
    public function setData($data): self
    {
        $this->data = $data;

        return $this;
    }

    private function countryIsNotSpain(): bool
    {
        $spainCountry = Country::whereName('España')->firstOrFail();

        return $this->getCountry() !== $spainCountry->getId();
    }

    private function dniIsNotValid(mixed $value): bool
    {
        return !$this->nifValidator->isValidNif($value) &&
            !$this->nifValidator->isValidCif($value) &&
            !$this->nifValidator->isValidNie($value);
    }

    private function getCountry(): ?int
    {
        return (int) ($this->data['country'] ?? 0);
    }
}
