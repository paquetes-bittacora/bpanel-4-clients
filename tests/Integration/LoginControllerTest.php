<?php

namespace Bittacora\Bpanel4\Clients\Tests\Integration;

use Bittacora\Bpanel4\Products\Tests\Acceptance\SetsUpApplication;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class LoginControllerTest extends TestCase
{
    use RefreshDatabase;
    use SetsUpApplication;

    protected function setUp(): void
    {
        parent::setUp();
        $this->createCountryAndState();
    }

    public function testCargaLaPaginaDeLogin(): void
    {
        $request = $this->get('/iniciar-sesion');
        $request->assertOk();
        $request->assertSee('Iniciar sesión');
    }
}
