<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Tests\Integration\Mail;

use Bittacora\Bpanel4\Clients\Database\Factories\ClientFactory;
use Bittacora\Bpanel4\Clients\Mail\ClientAccountActivatedMail;
use Illuminate\Foundation\Testing\RefreshDatabase;
use ReflectionException;
use Tests\TestCase;

final class ClientAccountActivatedMailTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @throws ReflectionException
     */
    public function testSeConstruyeElEmail(): void
    {
        $client = (new ClientFactory())->createOne();
        $mailable = new ClientAccountActivatedMail($client);

        $html = $mailable->render();

        self::assertStringContainsString('Su cuenta ha sido activada', $html);
    }
}
