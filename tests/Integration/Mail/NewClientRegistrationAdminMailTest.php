<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Tests\Integration\Mail;

use Bittacora\Bpanel4\Clients\Database\Factories\ClientFactory;
use Bittacora\Bpanel4\Clients\Mail\NewClientRegistrationAdminMail;
use Bittacora\Bpanel4\Clients\Models\Client;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use ReflectionException;
use Tests\TestCase;

final class NewClientRegistrationAdminMailTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @throws ReflectionException
     */
    public function testElEmailContieneElEnlaceParaEditarElCliente(): void
    {
        /** @var UrlGenerator $urlGenerator */
        $urlGenerator = $this->app->make(UrlGenerator::class);

        $client = $this->getClient();

        $mailable = new NewClientRegistrationAdminMail(
            $client,
            $this->app->make(Repository::class)
        );

        $html = $mailable->render();

        self::assertStringContainsString(
            $urlGenerator->route('bpanel4-clients.edit', ['client' => $client]),
            $html
        );
    }

    private function getClient(): Client
    {
        /** @var Client $client */
        $client = (new ClientFactory())->createOne();
        return $client;
    }
}
