<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Tests\Integration\Mail;

use Bittacora\Bpanel4\Clients\Database\Factories\ClientFactory;
use Bittacora\Bpanel4\Clients\Mail\NewClientRegistrationMail;
use Bittacora\Bpanel4\Clients\Models\Client;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use ReflectionException;
use Tests\TestCase;

final class NewClientRegistrationMailTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @throws ReflectionException
     */
    public function testSeConstruyeElEmail(): void
    {
        $client = $this->getClient();
        $mail = new NewClientRegistrationMail($client, $this->getConfigRepository());
        $html = $mail->render();
        $name = str_replace("'", '&#039;', $client->getName());
        self::assertStringContainsString($name . ', gracias por registrarse', $html);
    }

    private function getConfigRepository(): Repository
    {
        /** @var Repository $config */
        $config = $this->app->make(Repository::class);
        return $config;
    }

    private function setClientType(Client $client, string $clientType): void
    {
        $client->save();
        $client->refresh();
    }

    private function getClient(): Client
    {
        /** @var Client $client */
        $client = (new ClientFactory())->createOne();
        return $client;
    }
}
