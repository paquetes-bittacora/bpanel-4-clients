<?php

/** @noinspection PhpPossiblePolymorphicInvocationInspection */

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Tests\Integration;

use Bittacora\Bpanel4\Addresses\Database\Factories\ModelAddressFactory;
use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Bittacora\Bpanel4\Clients\Database\Factories\ClientFactory;
use Bittacora\Bpanel4\Clients\Models\Client;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class ClientAddressesTest extends TestCase
{
    use RefreshDatabase;

    public function testDevuelveUnaDireccionDeEnvioPorDefecto(): void
    {
        $client = $this->getClient();
        $firstAddress = $client->addresses()->firstOrFail();
        self::assertEquals($firstAddress->getId(), $client->getShippingAddress()->getId());
    }

    public function testSePuedeCambiarLaDireccionDeEnvioDelCliente(): void
    {
        $client = $this->getClient();
        $address = $client->addresses()->get()[2];
        assert($address instanceof ModelAddress);
        $client->setShippingAddress($address);
        self::assertEquals($address->getId(), $client->getShippingAddress()->getId());
    }

    public function testDevuelveLaDireccionDeEnvioComoDireccionDeFacturacionPorDefecto(): void
    {
        $client = $this->getClient();
        self::assertEquals(
            $client->getShippingAddress()->getId(),
            $client->getBillingAddress()->getId()
        );
    }

    public function testSePuedeCambiarLaDireccionDeFacturacion(): void
    {
        $client = $this->getClient();
        $address = $client->addresses()->get()[1];
        assert($address instanceof ModelAddress);
        $client->setBillingAddress($address);
        self::assertEquals($address->getId(), $client->getBillingAddress()->getId());
    }

    private function getClient(): Client
    {
        return (new ClientFactory())->has((new ModelAddressFactory())->count(3), 'addresses')
            ->create();
    }
}
