<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Tests\Integration;

use Bittacora\Bpanel4\Clients\Database\Factories\ClientFactory;
use Bittacora\Bpanel4\Clients\Models\Client;
use Bittacora\LaravelTestingHelpers\Permissions\PermissionTestHelper;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class MyAccountControllerTest extends TestCase
{
    use RefreshDatabase;

    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutExceptionHandling();

        $this->client = (new ClientFactory())->createOne();
        PermissionTestHelper::givePermissionTo($this->client->getUser(), 'my-account.manage-addresses');
        $this->actingAs($this->client->getUser());
    }

    public function testSeMuestraUnBotonParaAnadirUnaNuevaDireccion(): void
    {
        // Act
        $result = $this->get('/mi-cuenta/' . $this->client->getClientId() . '/direcciones');

        // Assert
        $result->assertSee('Añadir una dirección');
    }

    public function testSeMuestraLaVistaParaCrearUnaNuevaDireccion(): void
    {
        // Arrange

        // Act
        $result = $this->get('/mi-cuenta/direcciones/crear');

        // Assert
        $result->assertSee('name="address"', false);
        $result->assertSee('name="person_name"', false);
    }
}