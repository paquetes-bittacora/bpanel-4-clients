<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Tests\Integration\ObjectMothers;

use Bittacora\Bpanel4\Clients\Models\Client;
use Bittacora\Bpanel4\Clients\Repositories\ClientRepository;
use Bittacora\Bpanel4\Clients\UseCases\RegisterNewClient;
use Bittacora\LivewireCountryStateSelector\Models\Country;
use Bittacora\LivewireCountryStateSelector\Models\State;
use Bittacora\LivewireCountryStateSelector\Tests\Feature\Traits\CreatesCountryAndState;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Validation\ValidationException;
use Spatie\Permission\Models\Role;
use Throwable;

final class ClientObjectMother
{
    use CreatesCountryAndState;

    private readonly Generator $faker;

    public function __construct(
        private readonly RegisterNewClient $registerNewClient,
        private readonly ClientRepository $clientRepository,
    ) {
        $this->faker = Factory::create('es_ES');
        $this->createCountryAndState();
        Role::create(['name' => 'registered']);
    }

    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function getClient(): Client
    {
        $user = $this->registerNewClient->execute($this->getValidUserDataAsArray());
        return $this->clientRepository->getByUserId($user->getId());
    }

    /**
     * @return array<string, string>
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    private function getValidUserDataAsArray(): array
    {
        $password = $this->faker->password(8, 12);
        return [
            'name' => $this->faker->name,
            'surname' => $this->faker->lastName,
            'email' => $this->faker->email,
            'password' => $password,
            'password_confirmation' => $password,
            /** @phpstan-ignore-next-line  */
            'dni' => $this->faker->dni(),
            'phone' => $this->faker->phoneNumber,
            'country' => Country::whereName('España')->firstOrFail()->getId(),
            'state' => State::whereName('Badajoz')->firstOrFail()->getId(),
            'location' => $this->faker->city,
            'company' => 'Empresa',
            'address' => [
                'person_name' => $this->faker->name,
                'person_surname' => $this->faker->lastName,
                'name' => $this->faker->name,
                'location' => $this->faker->name,
                'address' => $this->faker->name,
                'postal_code' => '12345',
            ],
        ];
    }
}
