<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Tests\Integration\ObjectMothers;

use Bittacora\Bpanel4\Clients\Contracts\Client;
use Bittacora\Bpanel4\Clients\Contracts\ClientRepository;
use Bittacora\Bpanel4\Orders\Tests\Integration\Models\TestClient;
use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Bittacora\Bpanel4Users\Models\User;
use Bittacora\LivewireCountryStateSelector\Tests\Feature\Traits\CreatesCountryAndState;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Validation\ValidationException;
use Spatie\Permission\Models\Role;
use Throwable;

final class TestClientObjectMother
{
    use CreatesCountryAndState;

    private readonly Generator $faker;

    public function __construct(private readonly ClientRepository $clientRepository)
    {
        $this->faker = Factory::create('es_ES');
        $this->createCountryAndState();
        Role::create(['name' => 'registered']);
    }

    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function getClient(): Client
    {
        $user = $this->registerNewClient($this->getValidUserDataAsArray());
        return $this->clientRepository->getByUserId($user->getId());
    }

    private function registerNewClient(array $input): User
    {
        $user = User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => $input['password'],
        ]);

        $testClient = TestClient::create([
            'user_id' => $user->getId(),
            'name' => $input['name'],
            'surname' => $input['surname'],
            'dni' => $input['dni'],
            'company' => $input['company'],
            'phone' => $input['phone'],
        ]);

        $address = ModelAddress::create([
            'country_id' => $input['country'],
            'state_id' => $input['state'],
            'name' => $input['address']['name'],
            'address' => $input['address']['address'],
            'location' => $input['address']['location'],
            'postal_code' => $input['address']['postal_code'],
            'addressable_type' => Client::class,
            'addressable_id' => $testClient->id,
        ]);
        $testClient->shippingAddress()->associate($address)->save();
        $testClient->shippingAddress()->associate($address)->save();
        return $user;
    }

    /**
     * @return array<string, string>
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    private function getValidUserDataAsArray(): array
    {
        $password = $this->faker->password(8, 12);
        return [
            'name' => $this->faker->name,
            'surname' => $this->faker->lastName,
            'email' => $this->faker->email,
            'password' => $password,
            'password_confirmation' => $password,
            /** @phpstan-ignore-next-line */
            'dni' => $this->faker->dni(),
            'phone' => $this->faker->phoneNumber,
            'country' => 1,
            'state' => 1,
            'location' => $this->faker->city,
            'company' => 'Empresa',
            'address' => [
                'name' => $this->faker->name,
                'location' => $this->faker->name,
                'address' => $this->faker->name,
                'postal_code' => '12345',
            ],
        ];
    }
}
