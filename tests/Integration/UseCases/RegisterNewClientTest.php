<?php

/** @noinspection UnnecessaryAssertionInspection */

namespace Bittacora\Bpanel4\Clients\Tests\Integration\UseCases;

use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Bittacora\Bpanel4Users\Models\User;
use Bittacora\Bpanel4\Clients\Mail\NewClientRegistrationMail;
use Bittacora\Bpanel4\Clients\Mail\NewClientRegistrationAdminMail;
use Bittacora\Bpanel4\Clients\Models\Client;
use Bittacora\Bpanel4\Clients\UseCases\RegisterNewClient;
use Bittacora\LivewireCountryStateSelector\Models\Country;
use Bittacora\LivewireCountryStateSelector\Tests\Feature\Traits\CreatesCountryAndState;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;
use Spatie\Permission\Models\Role;
use Tests\TestCase;
use Throwable;

final class RegisterNewClientTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    use CreatesCountryAndState;

    private RegisterNewClient $registerNewClient;

    public function setUp(): void
    {
        parent::setUp();
        $this->createCountryAndState();
        $this->faker->locale = 'es_ES';
        Mail::fake();
        $this->registerNewClient = $this->app->make(RegisterNewClient::class);

        Role::findOrCreate('registered-user');
        Role::findOrCreate('any-user');
    }

    /**
     * @throws Throwable
     */
    public function testUnUsuarioSePuedeRegistrarSiLosDatosSonValidos(): void
    {
        $user = $this->registerNewClient->execute($this->getValidUserDataAsArray());

        $client = Client::where('user_id', $user->getId())->firstOrFail();

        /** @phpstan-ignore-next-line Siempre será true, pero lo dejo de todas formas por si hay cambios */
        self::assertInstanceOf(ModelAddress::class, $client->getShippingAddress());
        self::assertInstanceOf(User::class, $client->user()->first());
    }

    /**
     * @dataProvider fieldDataProvider
     * @throws Throwable
     */
    public function testUnUsuarioNoSePuedeRegistrarSiFaltaAlgunDatoObligatorio(string $fieldToTest): void
    {
        $this->expectException(ValidationException::class);
        $data = $this->getValidUserDataAsArray();
        unset($data[$fieldToTest]);

        $this->registerNewClient->execute($data);
    }

    /**
     * @throws Throwable
     */
    public function testUnUsuarioNoSePuedeRegistrarSiElDniNoEsValido(): void
    {
        $this->expectException(ValidationException::class);
        $data = $this->getValidUserDataAsArray();
        $data['dni'] = 'esto no es un dni válido';

        $this->registerNewClient->execute($data);
    }

    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function testEnviaEmailDeBienvenida(): void
    {
        $this->registerNewClient->execute($this->getValidUserDataAsArray());
        Mail::assertSent(NewClientRegistrationMail::class);
    }

    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function testEnviaMailAlAdministradorSiSeRegistraUnNuevoProfesional(): void
    {
        $this->registerNewClient->execute($this->getValidUserDataAsArray('professional'));
        Mail::assertSent(NewClientRegistrationAdminMail::class);
    }

    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function testLasDireccionesDelClienteSeCreanConElDni(): void
    {
        // Arrange
        $clientData = $this->getValidUserDataAsArray('professional');

        // Act
        $user = $this->registerNewClient->execute($clientData);
        $client = Client::where('user_id', $user->getId())->firstOrFail();

        // Assert
        $this->assertEquals($clientData['dni'], $client->getShippingAddress()->getNif());
    }

    /**
     * @return array<string, string>
     */
    private function getValidUserDataAsArray(?string $clientType = null): array
    {
        $password = $this->faker->password(8, 12);

        return [
            'name' => $this->faker->name,
            'surname' => $this->faker->lastName,
            'email' => $this->faker->email,
            'password' => $password,
            'password_confirmation' => $password,
            'dni' => $this->faker('es_ES')->dni(),
            'phone' => $this->faker->phoneNumber,
            'country' => Country::whereName('España')->firstOrFail()->id,
            'state' => 1,
            'location' => $this->faker->city,
            'postal_code' => $this->faker->numberBetween(10000, 99999),
            'company' => '',
            'address' => [
                'person_name' => $this->faker->name,
                'person_surname' => $this->faker->lastName,
                'name' => $this->faker->name,
                'location' => $this->faker->name,
                'address' => $this->faker->name,
                'postal_code' => '12345',
            ],
        ];
    }

    /**
     * @return string[][]
     * @noRector Se usa como dataProvider y rector no lo detecta
     */
    public static function fieldDataProvider(): array
    {
        return [
            'name' => ['name'],
            'surname' => ['surname'],
            'email' => ['email'],
            'password' => ['password'],
            'password_confirmation' => ['password_confirmation'],
            'dni' => ['dni'],
            'phone' => ['phone'],
            'country' => ['country'],
            'state' => ['state'],
            'address' => ['address'],
        ];
    }
}
