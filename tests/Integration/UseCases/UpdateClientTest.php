<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Tests\Integration\UseCases;

use Bittacora\Bpanel4\Clients\Mail\ClientAccountActivatedMail;
use Bittacora\Bpanel4\Clients\Repositories\ClientRepository;
use Bittacora\Bpanel4\Clients\Tests\Integration\ObjectMothers\ClientObjectMother;
use Bittacora\Bpanel4\Clients\UseCases\UpdateClient;
use Bittacora\LivewireCountryStateSelector\Models\Country;
use Bittacora\LivewireCountryStateSelector\Tests\Feature\Traits\CreatesCountryAndState;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;
use Spatie\Permission\Models\Role;
use Tests\TestCase;
use Throwable;

final class UpdateClientTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    use CreatesCountryAndState;

    /**
     * @var ClientObjectMother
     */
    private mixed $clientObjectMother;

    private ClientRepository $clientRepository;

    /**
     * @var UpdateClient
     */
    private mixed $updateClient;

    protected function setUp(): void
    {
        parent::setUp();

        Mail::fake();
        $this->createCountryAndState();
        $this->faker->locale = 'es_ES';
        $this->updateClient = $this->app->make(UpdateClient::class);
        $this->clientObjectMother = $this->app->make(ClientObjectMother::class);
        $this->clientRepository = $this->app->make(ClientRepository::class);

        Role::findOrCreate('nobody');
        Role::findOrCreate('registered-user');
        Role::findOrCreate('any-user');

        // Necesario para la validación del DNI
        $spain = new Country();
        $spain->name = 'España';
        $spain->save();
    }

    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function testElUsuarioPuedeActualizarSusDatosSinCambiarLaContrasena(): void
    {
        $client = $this->clientObjectMother->getClient();

        $postalCode = $this->faker->randomNumber(5);
        $data = $this->getClientData($client, $postalCode);

        $this->updateClient->execute($data);

        $updatedClient = $this->clientRepository->getById($client->id);

        self::assertEquals('Nombre modificado', $updatedClient->getName());
        self::assertEquals('email@modificado.com', $updatedClient->getUser()->getEmail());
        self::assertEquals($postalCode, $updatedClient->getShippingAddress()->postal_code);
    }

    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function testElUsuarioPuedeCambiarSuContrasena(): void
    {
        $client = $this->clientObjectMother->getClient();

        $oldPassword = $client->getUser()->getPassword();

        $data = [
            'client_id' => $client->id,
            'name' => $client->getName(),
            'surname' => $client->surname,
            'dni' => $client->dni,
            'company' => $client->getCompany(),
            'phone' => $client->getPhone(),
            'email' => $client->getUser()->getEmail(),
            'location' => $client->getShippingAddress()->getLocation(),
            'postal_code' => $client->getShippingAddress()->getPostalCode(),
            'password' => 'Contraseña nueva',
            'password_confirmation' => 'Contraseña nueva',
            'country' => $client->getShippingAddress()->getCountry()->getId(),
            'state' => $client->getShippingAddress()->getState()->getId(),
            'address' => [
                'name' => $this->faker->name,
                'location' => $this->faker->name,
                'address' => $this->faker->name,
                'postal_code' => '12345',
            ],
        ];

        $this->updateClient->execute($data);

        $updatedClient = $this->clientRepository->getById($client->id);

        self::assertNotEquals($oldPassword, $updatedClient->getUser()->getPassword());
    }

    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function testElUsuarioRecibeUnEmailCuandoSeActivaSuCuenta(): void
    {
        $client = $this->clientObjectMother->getClient();
        $user = $client->getUser();
        $user->setActive(0);
        $user->save();

        $clientData = $this->getClientData($client, 12345);
        $clientData['active'] = 1;
        $this->updateClient->execute($clientData);

        Mail::assertSent(ClientAccountActivatedMail::class);
    }

    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function testElUsuarioNoRecibeUnEmailCuandoSeDesactivaSuCuenta(): void
    {
        $client = $this->clientObjectMother->getClient();

        $clientData = $this->getClientData($client, 12345);
        $clientData['active'] = 0;
        $this->updateClient->execute($clientData);

        Mail::assertNotSent(ClientAccountActivatedMail::class);
    }

    /**
     * @return array<string, mixed>
     */
    private function getClientData(\Bittacora\Bpanel4\Clients\Models\Client $client, int $postalCode): array
    {
        return [
            'client_id' => $client->id,
            'name' => 'Nombre modificado',
            'surname' => 'Apellidos modificados',
            'dni' => $this->faker('es_ES')->dni(),
            'company' => 'Empresa modificada',
            'phone' => '612634656',
            'email' => 'email@modificado.com',
            'location' => $this->faker->city,
            'country' => 1,
            'state' => 1,
            'address' => [
                'name' => $this->faker->name,
                'location' => $this->faker->name,
                'address' => $this->faker->name,
                'postal_code' => $postalCode,
            ],
        ];
    }
}
