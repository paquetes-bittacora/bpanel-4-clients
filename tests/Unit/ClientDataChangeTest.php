<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Clients\Tests\Unit;

use Bittacora\Bpanel4\Clients\Database\Factories\ClientFactory;
use Bittacora\Bpanel4\Clients\Models\Client;
use Bittacora\Bpanel4Users\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class ClientDataChangeTest extends TestCase
{
    use RefreshDatabase;

    public function testAlBorrarUnClienteSeAnulanElDniNombreDeUsuarioYEmail(): void
    {
        // Arrange
        $client = (new ClientFactory())->createOne();
        $dni = $client->dni;
        $email = $client->getUser()->getEmail();
        $userName = $client->getUser()->getName();

        // Act
        $client->delete();

        // Assert
        $this->assertDatabaseHas(Client::class, [
            'dni' => $dni . '_DELETED'
        ]);
        $this->assertDatabaseHas(User::class, [
            'email' => $email . '_DELETED',
            'name' => $userName . '_DELETED',
        ]);
    }
}